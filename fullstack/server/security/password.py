from passlib.hash import argon2

def calc_hash(usersPassword):
    # Calculating a hash
    hash = argon2.using(rounds=4).hash(usersPassword)
    return hash

def validate_pass(usersPassword, hash_str):
    # Validating a hash
    if argon2.verify(usersPassword, hash_str):
        return True 
    else: 
        return False
