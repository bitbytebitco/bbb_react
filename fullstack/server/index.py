import os
import ssl
import json
from functools import wraps
import tornado.ioloop
import tornado.web, tornado.httpserver
import datetime
from datetime import timedelta
import logging
import time 
import markdown2
#import uimodules
from operator import itemgetter
from harvest_api import client
from security import password as pass_hash
from dbhandler import DB
#import email_sender
from configparser import SafeConfigParser

import stripe
from plaid import Client

parser = SafeConfigParser()
parser.read('/home/beckerz/configs/bbbr.ini')

# LOGGING
logger = logging.getLogger('bbb_logger')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('bbb.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to logger
logger.addHandler(ch)
logger.addHandler(fh)

# STRIPE 
DEBUG=parser.get("app_state","debug")
if int(DEBUG) == 1:
        print("### DEBUG MODE ###")
        stripe_config="stripe_test" 
        plaid_config="plaid_test"
else:
        print("### LIVE MODE ###")
        stripe_config="stripe_live" 
        plaid_config="plaid_dev"

LISTENING_PORT=parser.get("app_state","listening_port")
STRIPE_API_KEY=parser.get(stripe_config, "STRIPE_API_KEY")
STRIPE_PUBLISHABLE_KEY=parser.get(stripe_config, "STRIPE_PUBLISHABLE_KEY")

PLAID_CLIENT_ID=parser.get(plaid_config,"PLAID_CLIENT_ID")
PLAID_SECRET=parser.get(plaid_config,"PLAID_SECRET")
PLAID_PUBLIC_KEY=parser.get(plaid_config,"PLAID_PUBLIC_KEY")
PLAID_ENV=parser.get(plaid_config,"PLAID_ENV")

SITE_DIR = '/var/www/bbb_react/'

projects = [ 
    {
        "id":'whhw',
        "priority":0,
        "name": "Artist's Webcart", 
        "sub": "Website Development, Payment Gateway Integration, Hosting", 
        "logo":"Wilson_Wyllie.png",
        "screenshots": ["whhw_ss.png","whhw_ss2.png", "whhw_ss3.png", "whhw_ss4.png"],
        "url":"http://wilsonhhwyllie.com", 
        "section":["web"],
        "stack":{"Frontend":"HTML5/CSS, Bootstrap", "Backend":"Python", "Payment":"Stripe" }
    },
    { 
        "id":'data-visualizations',
        "priority":2,
        "name": "Visualizations Examples", 
        "sub": "Frontend/Backend, Stripe Integration", 
        "logo-glyph":'<i class="fas fa-chart-line"></i>',
        "screenshots": ["vis_screenshot.png"],
        "url":"http://charts.diluo.org", 
        "descr":"Here are some examples of visualizations Bit Byte Bit has provided, which may not be available for viewing, due to contract specifications. Examples include: <b>comparison of credits/debits</b> (line graphs, searchable by date span, with supporting line items), and a <b>GIS</b> example (shape file projections through D3.js)",
        "section":["web","data-vis"],
        "stack":{"Frontend":"HTML5/CSS, Bootstrap", "Charting Tools":"D3.js, Chart.js" }
    },
    {
        "id": 'acorn-inn',
        "priority":4,
        "name": "Acorn Inn", 
        "sub": "Website Development, Adaptive Bitrate Video, Hosting", 
        "logo":"acorn_inn_logo.jpg",
        "screenshots":["acorn_inn_ss.png"],
        "url":"http://acorninn.com", 
        "descr":"<span class='bbb'>BBB</span> provided a website, with mobile screen responsiveness, for The Acorn Inn. This site focused on showing the Inn's environment (through image galleries), as well as displaying lodging options and rates.",
        "section":["web"],
        "stack":{"Backend":"Python","Frontend":"HTML5/CSS, Bootstrap", "Database": "MySQL", "Video Editing":"ffmpeg"}
    },
    {
        "id":'rostovs',
        "priority":2,
        "name": "Rostov's Coffee & Tea", 
        "sub": "Android Native Application, Web Application Admin Portal", 
        "logo":"rostovs_logo.png",
        "screenshots":["rostovs_app.png"],
        "url":"", 
        "descr":"Rostov's is an excellent Coffee & Tea provider in Richmond, Virginia. Rostov's also has a coffee bar, which previously had a paper system for handling coffee bar account tabs. Bit Byte Bit replaced the paper system with a Native Android Application (and supporting infrastructure) solution. Now Rostov's can record transactions charged to customer's tabs, manage customers accounts, and monitor transactions through an Android tablet and supporting Web Application.",
        "section":["web","saas","data-vis","api"],
        "stack":{"Backend":"Python","Mobile Frontend": "Android Java", "Web App Frontend":"HTML5/CSS, Bootstrap", "Database": "MySQL"}
    },
    {
        "id":'filedrop',
        "priority":4,
        "name": "FileDrop", 
        "sub": "Frontend/Backend, Login with Google", 
        "logo":"filedrop_logo.png",
        "logo_class":"w-50",
        "screenshots":["filedrop_ss.png"],
        "url":"https://filedrop.diluo.org", 
        "descr":"FileDrop is a service for transferring files between individuals and groups of people. Built using Google's login SDK and Amazon's Simple Storage Service, this application was the solution to the internal need to pass sensitive and large documents around with.",
        "section":["saas","api"],
        "stack":{"Backend":"Python","Web App Frontend":"HTML5/CSS, Javascript, Bootstrap", "Database": "MySQL", "3rd Party API":"Google+, Amazon S3"}
    },
    {
        "id":'bjjpath',
        "priority":1,
        "name": "BJJ Path", 
        "sub": "SaaS Application Development, Branding, Payment Gateway Integration, Content Migration, Hosting", 
        "logo":"bjjpath_wide.png",
        "screenshots":["bjjpath_screenshot.png"],
        "url":"https://bjjpath.com", 
        "descr":"BJJ Path is a video subscription platform, comprised of Brazilian Jiu-Jitsu instructional material. This project involved building out the platform, business concept development, branding, and content migration.",
        "section":["web", "saas", "api", "data-scraping"],
        "stack":{"Backend":"Python","Frontend":"HTML5/CSS, JavaScript, Bootstrap" }
    },
    {
        "id":'dht11',
        "priority":5,
        "name": "DHT11 Temp Monitor", 
        "sub": "AVR, Microcrontroller", 
        "logo":"",
        "screenshots":["dht_monitor.jpg"],
        "url":"", 
        "descr":"",
        "section":["iot"],
        "stack":{"Platform":"AVR","Language":"AVR-C" }
    },
    #{
    #"id":'surveillance',
    #"priority":2,
        #"name": "Video Surveillance", 
    #    "sub": "Frontend/Backend, Stripe Integration", 
        #"logo":"/file/logo.png",
    #    "screenshots":["bjjpath_screenshot.png"],
        #"logo-glyph":'<i class="fas fa-camera-retro"></i>',
        #"url":"https://diluo.org:8878/stream", 
        #"descr":"<span class='bbb'>BBB</span> has been building a surveillance system, which spawned out of a pet project, which live streams video to a browser, so that the client may monitor the activity in their place of business. Streaming has very little time delay (seconds). Video is pulled in multiple video format, one for saving, and another for streaming. Each frame of video has the timestamp information encoded into it's repsective chunk of data, which is displayed on through the browser. Software motion detection has been implemented, and is currently in development for settings related to zones, motion overlay, etc.",
    #"section":["business", "iot"],
    #"stack":{"Backend":"Python","Frontend":"HTML5/CSS, JavaScript, Bootstrap", "Encodings":"mjpeg, h264" }
    #},
    {
    "id":'ric-data-map',
    "priority":3,
        "name": "Richmond Data Map", 
    "sub": "Application Development, Concept Development, Web Scraping, Hosting", 
        "logo":"rcw_icon.png",
    "logo_class":"w-50",
    "screenshots":["diluo_ss.png"],
        "url":"https://diluo.org", 
        "descr":"Richmond Data Map is a service that provides crime events from the City of Richmond. Crime events can be searched by date, neighborhood, and event type. All points are marked on an embedded Google Map for viewing. Some data review is also provided.",
    "section":["saas", "data-vis","api", "data-scraping"],
    "stack":{"Backend":"Python","Mobile Frontend": "Android Java (in development)", "Web App Frontend":"HTML5/CSS, JavaScript, Bootstrap", "Database": "MongoDB", "API": "Google Maps API"}
    },
    #{
    #"id":'bbb',
    #"priority":2,
        #"name": "Bit Byte Bit", 
        #"logo":"/file/logo.png",
    #    "screenshots":"bjjpath_screenshots.png",
        #"url":"https://bitbytebit.co", 
        #"descr":"This website was built internally to accommodate a few different objectives. Presenting marketing material was the primary need, but this has expanded. This site now also provides a portal for clients to view business documents, make payments on invoices, view invoices. As well the site provides a dashbaord for the business operator to see dynamic charts of payment history, project information, among other things. To make this possible Stripe, Plaid, and Harvest APIs are implemented.",
    #"section":["business"],
    #"stack":{"Backend":"Python","Frontend":"HTML5/CSS, Bootstrap", "Database": "MySQL", "3rd Party API": "Harvest API, Stripe, Plaid", "Other":"Markdown", "Charting Tools":"Chart.js"}
    #},
    #'luckys': {
#       "name": "Lucky's Bicycles", 
#       "logo":"/file/luckys_logo.png",
#       "url":"http://luckysbicycles.com/", 
#       "descr":"<span class='bbb'>BBB</span> provided a content management based (Drupal) website for Lucky's Bicycles to give them the ability to control the content without assistance. Lucky's Bicycles is a locally owned and operated business in the Richmond Area.",
#    "section":["business"]
#    }
]

#proj_ids = ['rostovs', 'filedrop', 'ric-data-map', 'luckys','acorn-inn', ]     
#proj_ids = [k for k in projects.keys()]        

class BaseHandler(tornado.web.RequestHandler):
    def initialize(self):
        if hasattr(self, 'db'):
            self.db.cnx.close()
        self.db = DB()

    def on_finish(self):
        try:
            if hasattr(self, 'db'):
                self.db.cnx.close()
        except Exception as e:
            print(e)

    def prepare(self):
        now = datetime.datetime.now()
        self.year = now.year

    def get_current_user(self):
        user_str = self.get_secure_cookie("user")
        if user_str:
            return json.loads(user_str.decode("utf-8"))

    def update_secure_cookie(self, **kwargs):
        user_str = self.get_secure_cookie("user")
        if user_str is not None:
            usr = json.loads(user_str)
        else: 
            usr = {
                "email":None, 
                "roles":None, 
                "harvest_client_id":None,
                "stripe_customer_id":None,
                "projects":None
            }

        if 'email' in kwargs:
            usr['email'] = kwargs['email']
        if 'roles' in kwargs:
            usr['roles'] = kwargs['roles']
        if 'harvest_client_id' in kwargs:
            usr['harvest_client_id'] = kwargs['harvest_client_id']
        if 'stripe_customer_id' in kwargs:
            usr['stripe_customer_id'] = kwargs['stripe_customer_id']
        if 'projects' in kwargs:
            usr['projects'] = kwargs['projects']
        self.set_secure_cookie("user", tornado.escape.json_encode(usr))

class ProjectsJsonHandler(BaseHandler):
    def get(self):
        self.write(json.dumps(projects))    

class ProjectJsonHandler(BaseHandler):
    def get(self, project_id):
        pJson = None
        for p in projects: 
            if project_id == p['id']:
                pJson = p
        self.write(json.dumps(pJson))    

class ProjectDetailHandler(BaseHandler):
    def get(self, project_id):
        self.render("index.html")

class ServiceInfo(BaseHandler):
    def _get_title(self, section):
        if section == "web":
            image = "web_development_md.png"
            icon_classes = "fas fa-desktop homeicon service-icon"
            title = "Web Development"
            tagline = "Web development for a variety of screen sizes."
            well_data = """<p>
            Does your business need web development suited for the modern era? <b>Bit Byte Bit</b> can build the tools to help your business reach audiences on all platforms. 
            <ul>
                <li>Bespoke Websites</li>
                <li>Custom Web Applications</li>
                <li>Custom Android Applications</li>
                <li>Payment Gateway Implementations
                <ul>
                    <li>Stripe</li>
                    <li>PayPal</li>
                    <li>Authorize.net</li>
                </ul>
                </li>
            </ul> 
            </p>
            """
        elif section == "iot":
            image = "signal_processing_md.png"
            icon_classes = "fas fa-signature homeicon service-icon"
            title = "Internet of Things"
            tagline = "Providing solutions to make sense of Big Data."
            well_data = ""
        elif section == "data-scraping":
            image = "data_scraping_md.png"
            icon_classes = "fa fa-filter homeicon service-icon"
            title = "Data Scraping"
            tagline = "Mining data in a variety of formats"
            well_data = """<p>Why do the tedius work that a computer can do more efficiently? 
            <b>BBB</b> can move data from many sources to a whatever output source you need.  
            <ul>
                <li>Website Scraping</li> 
                <li>Data Transfer</li> 
            </ul>
            </p> 
            """
        elif section == "saas":
            print('here we are')
            image = "saas_md.png"
            icon_classes = "fas fa-cogs homeicon service-icon"
            title = "SaaS"
            tagline = "Extending Services through the Digital Realm"
            well_data = """<p><b>Bit Byte Bit</b> can provide bespoke solutions to extend business services to:
            <ul>
                <li>Reach Markets Worldwide</li> 
                <li>Expedite Processes</li> 
                <li>Provide Better Organization for Complex Systems</li> 
            </ul>
            </p> 
            """
        elif section == "data-vis":
            image = "data_analytics_md.png"
            icon_classes = "fas fa-signal homeicon service-icon"
            title = "Data Visualization"
            tagline = "Providing solutions to make sense of Big Data."
            well_data = """<p>In a continually data-driven world having the ability to gather, organize, and make sense of data is essential. <b>Bit Byte Bit</b> can assist, with solutions including but not limited to:
            <ul>
                <li>Custom Visualizations</li>
                <li>Custom Administration Portals</li>
                <li>Custom Reporting in various formats
                <ul>
                    <li>Excel compatible formats</li>
                    <li>PDF</li>
                </ul>
                </li>
            </ul>
            </p>
            """
        elif section == "api":
            image = "api_md.png" 
            icon_classes = "fab fa-connectdevelop homeicon service-icon"
            title = "API Integration"
            tagline = "Connecting 3rd Party Tools for Added Leverage"
            well_data = """<div class="row"><div class="col-md-12">Rather than reinvent the wheel why not leverage existing tools that have the capability to connect to other systems? This is where Application Program Interfaces (API) come into play.</div>
            <div class="col-6 mt-4">
                <ul class="list-unstyled">
                    <li class="mb-2 pb-2"><b>Payment Gateways:</b>
                        <ul class="list-inline mt-2">
                            <li class="list-inline-item"><i class="fab fa-cc-stripe list-icons text-muted"></i></li>
                            <li class="list-inline-item"><i class="fab fa-squarespace list-icons text-muted"></i></li>
                            <li class="list-inline-item"><i class="fab fa-cc-paypal list-icons text-muted"></i></li>
                        </ul>     
                    </li>
                    <li class="mb-2 pb-2"><b>3rd Party Authentication:</b>
                        <ul class="list-inline mt-2">
                            <li><img src="https://bjjpath.com/static/img/google_signin2.png" class="gs" /></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-6 mt-4">
                <ul class="list-unstyled">
                    <li class="mb-2 pb-2"><b>File Storage:</b>
                        <ul class="list-inline mt-2">
                            <li class="list-inline-item"><i class="fab fa-aws list-icons text-muted"></i></li>
                            <li class="list-inline-item"><i class="fab fa-dropbox list-icons text-muted"></i></li>
                            <li class="list-inline-item"><i class="fab fa-google-drive list-icons text-muted"></i></li>
                        </ul>     
                    </li>
                </ul>
            </div>
            </div>
            """
        print(icon_classes)
        return title, tagline, well_data, image, icon_classes

    def get(self, section):
        try:
            if section != "all":
                title, tagline, well_data, image, icon_classes = self._get_title(section) 
                selected_projects = [] 
                for proj in projects:
                    #p = projects[proj]
                    print(proj)
                    if section in proj['section']:
                        selected_projects.append(proj)
                service_info = {
                    "title":title,
                    "tagline":tagline,
                    "well_data":well_data,
                    "image":image,
                    "icon_classes": icon_classes,
                    "projects":selected_projects
                }
            else:
                service_info = {
                    "title":"Work",
                    "tagline":tagline,
                    "well_data":"",
                    "icon_classes": icon_classes,
                    "projects":projects
                }
        except Exception as e:
            print('bad')
            print(e)
        self.write(json.dumps(service_info))

class ServiceHandler(BaseHandler):
    def get(self, section):
        self.render("index.html")

class LogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect('/')

class LoginHandler(BaseHandler):
    def get(self):
        print('try')
        print(self.xsrf_token)

        self.render("index.html", 
            page_title="Coming Soon", 
            year = self.year)

    def post(self):
        print("")
        print("###")
        print("LOGIN ATTEMPT")
        email = self.get_argument('email')
        password = self.get_argument('password')
        proceed=False
        user = self.db.select_user_by_email(email.strip())
        if user is not None:
            if pass_hash.validate_pass(password.strip(),user['password']):
                proceed = True 
            if proceed:
                roles = self.db.get_user_roles(user['id'])
                harvest_client_id = user['harvest_client_id']
                stripe_customer_id = user['stripe_customer_id']
                proceed=True 
                projects = client.get_projects_by_client_id(harvest_client_id)

                prj_list = []
                for prj in projects:
                    prj_list.append({'id':prj['id'], 'name':prj['name']})

                self.update_secure_cookie(email=email, roles=roles, harvest_client_id=harvest_client_id,
                        stripe_customer_id=stripe_customer_id, projects=prj_list)
                self.redirect('/admin/clients')
        else:
            self.clear()
            self.set_status(403)
            self.write(json.dumps({"error":"incorrect username/password"}))

class MainHandler(BaseHandler):
    def get(self):
        self.render("index.html")

class WorkHandler(BaseHandler):
    def get(self):
        self.render("index.html")

def checkACL(f):
    @wraps(f) 
    def wrapper(*args, **kwargs):
        print('checkACL')
        if isinstance(args[0], BaseHandler):
            if not hasattr(args[0], 'user'):
                args[0].user = args[0].get_current_user()
            if 'superuser' not in args[0].user['roles']:
                print('not a superuser')
                if isinstance(args[0], AdminClientsHandler):
                    url = "/admin/client/%s" % args[0].current_user['harvest_client_id'] 
                    args[0].redirect(url)
                if isinstance(args[0], AdminClientsInfoHandler):
                    args[0].set_status(403)
                    args[0].finish()
                elif isinstance(args[0], AdminProjectHandler):
                    user_prjs = [i['id'] for i in args[0].user['projects']]
                    if int(args[1]) in user_prjs:
                        print('authorized for viewing')
                    else:
                        print('uauthorized')
                        #raise tornado.web.HTTPError(403)
                        args[0].set_status(403)
                        #args[0].render('403.html')
                        args[0].finish()
                elif isinstance(args[0], AdminInvoiceHandler):
                    invoices = client.get_invoices_by_client_id(args[0].current_user['harvest_client_id'])
                    inv_ids = [int(i['id']) for i in invoices]
                    if int(args[1]) not in inv_ids:
                        #raise tornado.web.HTTPError(403)
                        args[0].set_status(403)
                        args[0].render('403.html')
                        #args[0].finish()
                elif isinstance(args[0], AdminClientEditHandler) or isinstance(args[0], AdminClientHandler):
                    if int(args[1]) != int(args[0].current_user['harvest_client_id']):
                        #raise tornado.web.HTTPError(403)
                        args[0].set_status(403)
                        args[0].render('403.html')
                        #args[0].finish()
        return f(*args, **kwargs)
    return wrapper

def checkAuth(f):
    @wraps(f) 
    def wrapper(*args, **kwargs):
        if isinstance(args[0], BaseHandler):
            args[0].user = args[0].get_current_user()
        return f(*args, **kwargs)
    return wrapper

class AdminBaseHandler(BaseHandler):
    @tornado.web.authenticated
    def prepare(self): 
        now = datetime.datetime.now()
        self.year = now.year

    def get_payment_methods(self):
        if self.current_user['stripe_customer_id'] is not None:
            try:
                payment_methods = {'bank_accts':None, 'cards':None} 
                stripe.api_key = STRIPE_API_KEY 
                accts = stripe.Customer.retrieve(self.current_user['stripe_customer_id']).sources.list(
                    limit=15, object="bank_account")
                payment_methods['bank_accts'] = accts
                return payment_methods 
            except Exception as e:
                print(e)
        else:
            return None

    def get_bank_acct(self, acct_id):
        stripe.api_key = STRIPE_API_KEY 
        customer = stripe.Customer.retrieve(self.current_user['stripe_customer_id'])
        return customer.sources.retrieve(acct_id)

class AdminHandler(AdminBaseHandler):
    def get(self):
        self.render("index.html")
        #self.redirect('/admin/clients')

class AdminClientsHandler(AdminBaseHandler):
    def _get_day_amts(self):
        
        stripe.api_key = STRIPE_API_KEY 
        days_ago = 30 
        d = datetime.datetime.today() - timedelta(days=days_ago)
        ndaysago = int(time.mktime(d.timetuple()))
        charges = stripe.Charge.list(created={'gt':ndaysago}, limit=100)
        data = [] 
        for i in reversed(range(days_ago)):
            d = datetime.datetime.today() - timedelta(days=i)
            #ndaysago = int(time.mktime(d.timetuple()))
            date_str =  d.strftime('%m-%d-%Y')
            total = sum((float(c['amount'])/100) for c in charges \
                if datetime.datetime.fromtimestamp(c['created']).strftime('%m-%d-%Y') == d.strftime('%m-%d-%Y')\
                )   
            data.append({'x': d.isoformat(), 'y':total})
        return data

    @checkACL
    @checkAuth
    def get(self):
            amts = self._get_day_amts() 
            
            '''cl = Client(PLAID_CLIENT_ID,
                    PLAID_SECRET,
                    PLAID_PUBLIC_KEY,
                    PLAID_ENV)
            accts = cl.Auth.get()'''
            clients = client.get_clients()
            active_projects=client.get_active_projects()
          
            todo_list = ['commercial', 'internal', 'personal'] 
            
            self.render('index.html', year=self.year, clients=clients, active_projects=active_projects,
                amts=json.dumps(amts), todo=todo_list) 

class AdminClientsInfoHandler(AdminBaseHandler):
    @checkACL
    @checkAuth
    def get(self):
        print('test')
        clients = client.get_clients()
        self.write(json.dumps(clients))

class AdminClientInfoHandler(AdminBaseHandler):
    @checkACL
    @checkAuth
    def get(self, client_id):
        cl = client.get_client(client_id)
        try:
            cl['contact'] = client.get_contact_by_id(client_id)
            print(cl['contact'])
        except Exception as e:
            print(e)
        cl['projects'] = client.get_projects_by_client_id(client_id)
        cl['invoices'] = client.get_invoices_by_client_id(client_id)
        cl['estimates'] = client.get_estimates_by_client_id(client_id)
        cl['payment_methods'] = self.get_payment_methods()
        print('client info')
        self.write(json.dumps(cl))


class AdminClientHandler(AdminBaseHandler):
    @checkACL
    @checkAuth
    def get(self, client_id):
        self.render('index.html') 

class AdminClientEditHandler(AdminBaseHandler):
    @checkACL
    @checkAuth
    def get(self, client_id):
        cl = client.get_client(client_id)
        cl['contact'] = client.get_contact_by_id(client_id)
        if 'pass_updated' in  self.request.arguments:
            pass_updated = self.get_argument('pass_updated') 
        else:
            pass_updated = None
                
        self.render('index.html')
        #self.render('admin-client-edit.html', year=self.year, client=cl, pass_updated=pass_updated, payment_methods=payment_methods, plaid_env=PLAID_ENV, plaid_public_key=PLAID_PUBLIC_KEY, stripe_pub_key=STRIPE_PUBLISHABLE_KEY)

    def _verify_account(self, last4, amt1,amt2):
        print("_verify_account")
        try:
            stripe.api_key = STRIPE_API_KEY 
            customer = stripe.Customer.retrieve(self.current_user['stripe_customer_id'])
            accts = stripe.Customer.retrieve(self.current_user['stripe_customer_id']).sources.list(
                    limit=15, object="bank_account")
            for acct in accts:
                if acct['last4'] == last4:
                    ba = bank_account = customer.sources.retrieve(acct['id'])
                    ba.verify(amounts = [amt1,amt2])
        except Exception as e:
            print(e)

    @checkACL
    @checkAuth
    def post(self, client_id):
        print('')
        print('')
        print('AdminClientEdit POST')
        print('')
        contact = client.get_contact_by_id(client_id)
        if 'verify_bank_account' in self.request.arguments:
            last4 = self.get_argument('last4')
            amt1 = self.get_argument('amount_1')
            amt2 = self.get_argument('amount_2')
            self._verify_account(last4, amt1, amt2)     
             
            if 'add_bank_acct' in self.request.arguments:
                    bank_account_token = self.get_argument('token')

                    stripe.api_key = STRIPE_API_KEY 
                    # retrieve customer or create one if nonexistent 
                    if self.current_user['stripe_customer_id'] is None:
                        desc = "Customer for %s" % self.current_user['email']
                        resp = stripe.Customer.create(
                                        description= desc ,
                                        source=bank_account_token # obtained with Stripe.js
                                        )
                        if 'id' in resp:
                                self.db.update_stripe_customer_id(self.current_user['email'], resp['id'])
                                self.update_secure_cookie(stripe_customer_id=resp['id'])
                    else:
                        # add source to stripe customer
                        customer = stripe.Customer.retrieve(self.current_user['stripe_customer_id'])
                        customer.sources.create(source=bank_account_token)
            elif 'first_name' in self.request.arguments:
                    # update contact
                    data = {
                            'first_name':self.get_argument('first_name'),
                            'last_name':self.get_argument('last_name'),
                            'phone_office':self.get_argument('phone_office')
                    }
                    resp = client.update_contact_by_id(contact['id'],data)
                    resp2 = client.update_client_by_id(client_id, {'address': self.get_argument('address')})
    
        url = "/admin/client/%s/edit" % client_id
        self.redirect(url) 

class AdminPaymentMethodsHandler(AdminBaseHandler):
    @checkAuth
    @checkACL
    def get(self):
        print('payment_methods')
        self.write(json.dumps(self.get_payment_methods()))

class AdminInvoiceInfoHandler(AdminBaseHandler):
    @checkAuth
    @checkACL
    def get(self, invoice_id):
        print('test')
        print(invoice_id)
        client_id = self.current_user['harvest_client_id']
        cl = client.get_client(client_id)
        invoice = client.get_invoice_by_id(invoice_id)
        print(invoice)
        self.write(json.dumps(invoice))

class AdminInvoiceHandler(AdminBaseHandler):
    @checkAuth
    @checkACL
    def get(self, invoice_id):
        #client_id = self.current_user['harvest_client_id']
        #cl = client.get_client(client_id)
        #invoice = client.get_invoice_by_id(invoice_id)
        self.render('index.html'); 
        #self.render('admin-invoice.html', year=self.year, client=cl, inv=invoice, payment_methods=self.get_payment_methods(), stripe_publishable_key=STRIPE_PUBLISHABLE_KEY) 

    @checkAuth
    @checkACL
    def post(self, invoice_id):
        print('')
        print('AdminInvoice: POST')
        print(invoice_id)

        amount = int(float(self.get_argument('amount')))

        stripe.api_key = STRIPE_API_KEY 
        if 'stripeTokenType' in self.request.arguments:
            #if token_type == "card":
            token = self.get_argument('stripeToken')
            token_type = self.get_argument('stripeTokenType')
            email = self.get_argument('stripeEmail')

            charge = stripe.Charge.create(
                    amount=amount,
                    currency='usd',
                    description='Example charge',
                    source=token,
                    receipt_email=email,
                    )
        elif 'token_type' in self.request.arguments:
            last4 = self.get_argument('last4')
            payment_methods = self.get_payment_methods()
            source_id = None
            for acct in payment_methods['bank_accts']['data']:
                if last4 == acct['last4']:
                    acct_id = acct['id']
            if acct_id is not None:
                #acct = self.get_bank_acct(acct_id)
                # having problems with providing proper id to source
                charge = stripe.Charge.create(
                        amount=amount,
                        currency="usd",
                        source=acct_id,
                        customer=self.current_user['stripe_customer_id'],
                        receipt_email=self.current_user['email'],
                        ) 
                print(charge)
        if charge['outcome']['network_status'] != 'approved_by_network':
            print('houston, prollems')
        else:
            description = charge['description']
            # create invoice payment harvest
            iso_date = datetime.datetime.fromtimestamp(charge['created']).isoformat()
            amt =  float(amount)/100
            data = {"amount":amt, "paid_at":iso_date, "notes":description}
            client.create_invoice_payment(invoice_id, data) 
            url = '/admin/client/%s' % self.current_user['harvest_client_id']
            self.redirect(url)


class AdminProjectHandler(AdminBaseHandler):
    @checkAuth
    @checkACL
    def get(self, project_id):
        project = client.get_project_by_id(project_id)
        #entries = client.get_time_entries_by_project_id(project_id)
        project['tasks'] = client.get_project_tasks_by_id(project_id)
        '''for task in project['tasks']['task_assignments']:
            #hours = client.get_hourly_total_by_task_id(task['id'], entries=entries)
            hours = client.get_hourly_total_by_task_id(task['id'], project_id=project_id)
            task['hours'] = hours
        '''
        project['hours_to_date'] = client.get_project_hours_to_date_by_project_id(project_id)
        self.render('admin-project.html', year=self.year, project=project)
        pass

class My404Handler(BaseHandler):
    # Override prepare() instead of get() to cover all possible HTTP methods.
    def prepare(self):
        self.set_status(404)
        now = datetime.datetime.now()
        self.render("404.html", year=now.year)
        
class IPlogger(BaseHandler):
    def get(self, *args):
        self.write('ok')

def make_app():
    settings = {
        "xsrf_cookies": True,
            "cookie_secret" : "dat$katYouKnowYouLoveItWhenIKick!T",
        "login_url":"/login",
        #"static_path": "../static/dist",
        "template_path": "../static"
                #"ui_modules": uimodules,
    }
    return tornado.web.Application(
        [
            (r"/", MainHandler),
            (r"/work", WorkHandler),
            (r"/service/(.*)", ServiceHandler),
            (r"/service_info/(.*)", ServiceInfo),
            (r"/project/(.*)", ProjectDetailHandler),
            (r"/login", LoginHandler),
            (r"/logout", LogoutHandler),
            (r"/admin", AdminClientsHandler),
            (r"/admin/clients", AdminClientsHandler), # checkACL
            (r"/admin/clients/info", AdminClientsInfoHandler), #        checkACL
            (r"/admin/client/(\d{7})", AdminClientHandler),      # checkACL
            (r"/admin/client_info/(\d{7})", AdminClientInfoHandler),     # checkACL
            (r"/admin/client/(\d{7})/edit", AdminClientEditHandler), # checkACL
            (r"/admin/invoice/(\d+)", AdminInvoiceHandler), # checkACL
            (r"/admin/invoice_info/(\d+)", AdminInvoiceInfoHandler),     # checkACL
            (r"/admin/payment_methods", AdminPaymentMethodsHandler),     # checkACL
            (r"/contact", MainHandler),
            (r"/json/projects", ProjectsJsonHandler),
            (r"/json/project/(.*)", ProjectJsonHandler),
            (r'/img/(.*)', tornado.web.StaticFileHandler, {'path': os.path.abspath(os.path.join(os.getcwd(), "..", "static", "images"))}),
            (r'/dist/(.*)', tornado.web.StaticFileHandler, {
            'path': os.path.abspath(os.path.join(os.getcwd(), "..", "static", "dist"))
            }),
            #(r'/js/(.*)', tornado.web.StaticFileHandler, {'path': SITE_DIR + "/js/"}),
            (r'/file/(.*)', tornado.web.StaticFileHandler, {'path': SITE_DIR + "/static/files/"}),
            (r'/(.*(pma|php|wp-content).*)', IPlogger),
            (r'/(.*.txt)', tornado.web.StaticFileHandler, {'path': SITE_DIR + "/public/"}), # handling ssl cert validation
        ], 
        debug=True, 
        default_handler_class=My404Handler, 
        **settings
    )


if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(
        make_app(),
    )
    http_server.listen(LISTENING_PORT) 
    tornado.ioloop.IOLoop.current().start()
