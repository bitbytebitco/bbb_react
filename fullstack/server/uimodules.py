import re
import tornado.web
import tornado.escape
from bs4 import BeautifulSoup

class Breadcrumbs(tornado.web.UIModule):
    def render(self, current_page=None, parent=None, btype=None):
        return self.render_string(
            "modules/breadcrumbs.html", current_page=current_page, parent=parent, btype=btype)

class AdminSideBar(tornado.web.UIModule):
    def render(self, current_page='home'):
        return self.render_string(
            "modules/admin-sidebar.html", current_page=current_page)

class SubscriberSideBar(tornado.web.UIModule):
    def render(self, current_page='home'):
        return self.render_string(
            "modules/subscriber/sidebar.html", current_page=current_page)

class Sidebar(tornado.web.UIModule):
    def render(self, current_page='home'):
        return self.render_string(
            "modules/frontend/sidebar.html", current_page=current_page)

class Navbar(tornado.web.UIModule):
    def render(self, current_page='home', article_id=None):
        return self.render_string(
            "modules/frontend/navbar.html", current_page=current_page, article_id=article_id)

class AddGroupForm(tornado.web.UIModule):
    def javascript_files(self):
        return "/static/js/admin-group-form.js" 
    def render(self, parent_id=None):
        return self.render_string("modules/add-group-form.html", parent_id=parent_id)


class AdminGroupList(tornado.web.UIModule):
    def render(self, groups):
        return self.render_string("modules/admin-group-list.html", groups=groups)

class AdminSubscribersList(tornado.web.UIModule):
    def render(self, subs):
        return self.render_string("modules/admin-subs-list.html", subs=subs)

class AnonVisitors(tornado.web.UIModule):
    def render(self, visitors):
        return self.render_string("modules/anon-visitors.html", visitors=visitors)

class ChildGroupList(tornado.web.UIModule):
    def render(self, groups):
        if groups is not None:
           groups = sorted(groups, key = lambda i:sorted(i.items()), reverse=True) 
        return self.render_string(
            "modules/frontend/child-group-list.html", groups=groups)

class ChildGroupRows(tornado.web.UIModule):
    def render(self, groups):
        if groups is not None:
           groups = sorted(groups, key = lambda i:sorted(i.items()), reverse=True) 
        return self.render_string(
            "modules/frontend/child-group-rows.html", groups=groups)

class TagRows(tornado.web.UIModule):
    def render(self, tags):
        return self.render_string(
            "modules/frontend/tag-rows.html", tags=tags)

class ArticlesList(tornado.web.UIModule):
    def render(self, articles):
        return self.render_string(
            "modules/frontend/article-list.html", articles=articles)

class MainCategories(tornado.web.UIModule):
    def render(self, cats):
        return self.render_string(
            "modules/frontend/main-categories.html", cats=cats)

class TopGroupRows(tornado.web.UIModule):
    def render(self, groups, fundamental=True):
        if fundamental:
            ordered = {} 
            if groups is not None:
                for g in groups:
                    if g['child_id'] is None: 
                        n = {"id":g['id'], "title":g['title']}
                        ordered[g['id']] = {"id":g['id'], "title":g['title'], "children":[], "featured_priority":g["featured_priority"]}
                    else:
                        n = {"id":g['child_id'], "title":g['child_title']}
                        if g['id'] not in ordered:
                            ordered[g['id']] = {"id":g['id'], "title":g['title'], "children":[], "featured_priority":g["featured_priority"]}
                        ordered[g['id']]['children'].append(n)
                ordered = sorted(ordered.values(), key = lambda x: x['featured_priority'], reverse=True)
        else:
            ordered = groups
        return self.render_string(
            "modules/frontend/top-group-rows.html", groups=ordered)

class GroupList(tornado.web.UIModule):
    def render(self, groups):
        return self.render_string(
            "modules/frontend/group-list.html", groups=groups)

class ArticlesByCat(tornado.web.UIModule):
    def render(self, category, articles):
        rows = None
        if category in articles:
           rows = articles[category][:5] 
        return self.render_string(
            "modules/frontend/articles-by-cat.html", category=category, articles=rows)

class ArticleViewsList(tornado.web.UIModule):
    def render(self, articles, short=False):
        return self.render_string(
            "modules/frontend/article-views-list.html", articles=articles, short=short)

class FeaturedArticlesBlocks(tornado.web.UIModule):
    def render(self, featured):
        return self.render_string(
            "modules/frontend/featured-articles-blocks.html", featured=featured)

## ADMIN

class CategoryRow(tornado.web.UIModule):
    '''def javascript_files(self):
        return "/static/js/categories.js" '''
    def render(self, categories, category_id=None):
        return self.render_string(
            "modules/category-row.html", categories=categories, category_id=category_id)

class ArticleGroupRow(tornado.web.UIModule):
    def render(self, groups, group_id=None):
        return self.render_string(
            "modules/article-group-row.html", groups=groups, group_id=group_id)

class TagRow(tornado.web.UIModule):
    def javascript_files(self):
        return "/static/js/tags.js" 
    def render(self, tags, all_tags):
        return self.render_string(
            "modules/tags.html", tags=tags, all_tags=all_tags)

class ArticleLinkRow(tornado.web.UIModule): # admin
    def javascript_files(self):
        return "/static/js/article_link.js" 
    def render(self, article_type, articles=None):
        if articles is not None:
            if article_type is not "group":
                arts = [a for a in articles if a['type'] == article_type] 
            else: 
                arts = articles
        else:
            arts = None
        return self.render_string("modules/article_link.html", article_type=article_type, articles=arts)

class GroupLinkRow(tornado.web.UIModule): # admin
    def javascript_files(self):
        return "/static/js/group_link.js" 
    def render(self, group_id=None, group_title=None):
        return self.render_string("modules/group_link.html", group_id=group_id, group_title=group_title)

class FeaturedGroupRow(tornado.web.UIModule):
    def render(self, featured=None):
        return self.render_string("modules/featured-group-option.html", featured=featured)

class FeaturedOptRow(tornado.web.UIModule):
    def render(self, featured_flag=None, blurb=None):
        return self.render_string("modules/featured.html", featured_flag=featured_flag, blurb=blurb)

class IntroPageOptRow(tornado.web.UIModule):
    def javascript_files(self):
        return "/static/js/set_intro.js" 
    def render(self, intro_page=None):
        return self.render_string("modules/intro_page_opt.html", intro_page=intro_page)

class LinkedArticlesTable(tornado.web.UIModule): # admin
    def render(self, article_type, articles=None):
        return self.render_string("modules/linked_articles_table.html", article_type=article_type, articles=articles)

class FEArticleLinks(tornado.web.UIModule): # frontend 
    def render(self, links):
        atypes = { l['type'] for l in links }
        return self.render_string("modules/frontend/article_links.html", atypes=atypes, links=links)

class FELinkSection(tornado.web.UIModule): # frontend 
    def render(self, atype, links):
        if links is not None:
            ls = [l for l in links if l['type'] == atype]
        else:
            ls = None
        return self.render_string("modules/frontend/article_link_section.html", atype=atype, links=ls)

class DismissableAlert(tornado.web.UIModule):
    def render(self, title, items, alert_id, show_btns=False):
        return self.render_string("modules/frontend/dismissable-alert.html",
            show_btns=show_btns,
            items=items,
            alert_id=alert_id,
            title=title)

class UpdatePasswordForm(tornado.web.UIModule):
    def render(self, user, update_pass):
        return self.render_string("modules/update-password-form.html",
            user=user,
            update_pass=update_pass)

class D3VisitorMap(tornado.web.UIModule):
    def render(self, data, world_data):
        return self.render_string("modules/d3/visitor_map.html", visitor_data=data, world_data=world_data)


class D3Heatmap(tornado.web.UIModule):
    def javascript_files(self):
        return "/static/js/heatmap_d3.js" 
    def render(self, data):
        return self.render_string("modules/d3/heatmap.html",
            heatmap_data=data
        )

class QuillEditor(tornado.web.UIModule):
    def javascript_files(self):
        return "/static/js/quill_logic.js" 
    def render(self, content=""):
        #print(content)
        content = content.replace("<p><br/></p>","")
        content = content.replace("<p><br></p>","<br>")
        #content = re.sub('[\s]+<', '<', content) # remove whitespaces before opening tags
        #content = re.sub('>[\s]+', '>', content) # remove whitespaces after closing tags
        return self.render_string("modules/quill_editor.html", content=tornado.escape.url_escape(content, plus=False))

class EmbedVideosFromToken(tornado.web.UIModule):
    def render(self, content, db):
        #print(content)
        for t in re.findall(r'\[video_id-\S+\]', content):
            #print(t)
            video_id = t.split('[video_id-')[1].replace(']','')
            #print(video_id)
            video_url = db.select_from('get_video', {"video_id":video_id})[0]['video_url']
            html_string =  self.render_string(
            "modules/video_embed_code.html", video_url=video_url)   
            #print('html_string')
            #print(html_string)
            content = content.replace(t, html_string.decode('utf-8')) 
        return content  

class EmbedVideosFromImgTag(tornado.web.UIModule):
    def rewrite_article_urls(self, content):
        #for a in re.findall('((https://videncyc.diluo.org/article)(/.*?)">)', content):
        #    print(a)"
        return content.replace("https://videncyc.diluo.org","")
        
    def render(self, content, db):
        content = self.rewrite_article_urls(content)
        content = content.replace("<p><br/></p>","")
        content = content.replace("<p><br></p>","")
        soup = BeautifulSoup(content)
        
        #print(content)

        for l in soup.find_all("a"):
            if 'https://howtheyplay.com/individual-sports/' in l.attrs['href']:
                l.attrs['href'] = l.attrs['href'].replace('https://howtheyplay.com/individual-sports/','')
        #for t in re.findall('(<img.*?(youtube_id-(.*?)">))', content):
        #for t in re.findall('((?:\?.*v=|\/))([a-zA-Z0-9\-_]+)', content):
        #for t in re.findall('(<img.*ref="?(?:youtube_id-(.{11})).*)">', content):
        for l in soup.find_all("img", {"ref":re.compile('.*youtube.*')}):
            youtube_id = l.attrs['ref'].replace('youtube_id-','')
            #wrap = soup.new_tag("div", class_="videoWrapper")
            #video_embed = soup.new_tag("iframe", class_="videoWrapper")
            video_url = "https://www.youtube.com/embed/{}?rel=0&modestbranding=1&showinfo=0".format(youtube_id)
            html_string =  self.render_string(
                "modules/video_embed_code.html", video_url=video_url)
            video_html = BeautifulSoup(html_string, features="html.parser")
            l.replace_with(video_html)
        """
        for t in re.findall('(<img.*?(?:youtube_id-(.{11})).*">)', content):
        #for t in re.findall('(<img.*ref=?"(?:youtube_id-(.{11})).*">)', content):
            print('')
            print('reMatch')
            print(t)
            #youtube_id = t.split('youtube_id-')[1].replace('"','')
            for g in t:
                #print('')
                #print('t:{}'.format(g))
                if len(g) == 11:
                    print('found: {}'.format(g))
                    youtube_id = g.strip()
            '''if 'youtube_id' not in t[2]:
                youtube_id = t[2] 
            else: 
                youtube_id = t[4] 
            '''

            #video_url = db.select_from('get_video', {"video_id":video_id})[0]['video_url']
            video_url = "https://www.youtube.com/embed/{}?rel=0&modestbranding=1&showinfo=0".format(youtube_id)
            print(video_url)
            html_string =  self.render_string(
                "modules/video_embed_code.html", video_url=video_url)   
            content = content.replace(t[0], html_string.decode('utf-8')) 
        """ 
        return soup  

class UploadImageUI(tornado.web.UIModule):
    def javascript_files(self):
        return "/static/js/imageUpload.js" 
    def render(self, heading):
        return self.render_string("modules/uploadFile.html", heading=heading)

class ModalUpdateCard(tornado.web.UIModule):
    def render(self):
        return self.render_string("modules/modal-update-card.html")

class LastViewsChart(tornado.web.UIModule):
    def render(self, chart_id, views, scale_type):
        return self.render_string("modules/last-views-chart.html", chart_id=chart_id, views=views, scale_type=scale_type) 

class LatestSubsChart(tornado.web.UIModule):
    def render(self, chart_id, subs):
        return self.render_string("modules/latest-subs-chart.html", chart_id=chart_id, subs=subs) 
