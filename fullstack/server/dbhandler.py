import os
import time
import mysql.connector
from security import password as pass_hash
from mysql.connector import errorcode

from configparser import SafeConfigParser

parser = SafeConfigParser()
parser.read('/home/beckerz/configs/bitbytebit.ini')

DATABASE=parser.get("mysql","database")
USER=parser.get("mysql","user")
PASS=parser.get("mysql","pass")

TABLES = {}

TABLES['users'] = (
        "CREATE TABLE `users` ("
        "  `id` int(11) NOT NULL AUTO_INCREMENT,"
        "  `first_name` varchar(40) NOT NULL,"
        "  `last_name` varchar(40) NOT NULL,"
        "  `harvest_client_id` int(11),"
        "  `email` varchar(40) NOT NULL,"
        "  `password` varchar(80) NOT NULL,"
        "  PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`)"
        ") ENGINE=InnoDB")

TABLES['roles'] = (
        "CREATE TABLE `roles` ("
        "  `id` int(11) NOT NULL AUTO_INCREMENT,"
        "  `name` varchar(40) NOT NULL,"
        "  PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`)"
        ") ENGINE=InnoDB")

TABLES['user_roles'] = (
        "CREATE TABLE `user_roles` ("
        "  `user_id` int(11) NOT NULL,"
        "  `role_id` int(11) NOT NULL,"
        "  PRIMARY KEY (`user_id`,`role_id`)"
        ") ENGINE=InnoDB")

TABLES['plaid_access_tokens'] = (
        "CREATE TABLE `plaid_access_tokens` ("
        "  `user_id` int(11) NOT NULL,"
        "  `access_token` varchar(60) NOT NULL,"
        "  `created` datetime NOT NULL,"
        "  PRIMARY KEY (`user_id`,`access_token`)"
        ") ENGINE=InnoDB")

TABLES['email_codes'] = (
        "CREATE TABLE `email_codes` ("
        "  `code` bigint UNSIGNED NOT NULL,"
        "  `email` varchar(40) NOT NULL,"
        "  `state` varchar(20) NOT NULL,"
        "  PRIMARY KEY (`code`), UNIQUE KEY `code` (`code`)"
        ") ENGINE=InnoDB")

SQL = {}

SQL['create_user'] = (
        "INSERT INTO users "
        "(first_name, last_name, harvest_client_id, email, password)"
        "VALUES (%(first_name)s, %(last_name)s, %(harvest_client_id)s, %(email)s, %(password)s)")

SQL['update_pass_by_email'] = (
        "UPDATE users set `password` = %(password)s"
        "WHERE email = %(email)s")

SQL['update_stripe_id_by_email'] = (
        "UPDATE users set `stripe_customer_id` = %(stripe_customer_id)s"
        "WHERE email = %(email)s")

SQL['select_user_by_email'] = (
        "SELECT * from users "
        "WHERE email = %(email)s")

SQL['select_user_by_harvest_id'] = (
        "SELECT * from users "
        "WHERE harvest_client_id = %(harvest_id)s")

SQL['create_role'] = (
        "INSERT INTO roles "
        "(name)"
        "VALUES (%(name)s)")

SQL['add_user_role'] = (
        "INSERT INTO user_roles "
        "(user_id,role_id)"
        "VALUES (%(user_id)s, %(role_id)s)")

SQL['select_role_by_name'] = (
        "SELECT * from roles "
        "WHERE name = %(name)s")

SQL['select_user_roles_by_user_id'] = (
        "SELECT r.name from user_roles ur JOIN roles r on ur.role_id=r.id "
        "WHERE ur.user_id = %(user_id)s")

SQL['select_all_users'] = ("SELECT * from users")

SQL['close_email_code'] = ("UPDATE email_codes set state='closed' where code = %(code)s")
SQL['create_email_code'] = (
        "INSERT INTO email_codes "
        "(code,email,state)"
        "VALUES (%(code)s,%(email)s,'open')")
SQL['get_email_code'] = (
        "SELECT * from email_codes "
        "WHERE code = %(code)s")

SQL['add_plaid_access_token'] = (
        "INSERT INTO plaid_access_tokens "
        "(user_id, access_token, created)"
        "VALUES (%(user_id)s, %(access_token)s, NOW())")


#SQL['select_all_users_nonadmin'] = ("SELECT * from users WHERE admin<>1")

class DB(object):
    def __init__(self):
        try: 
            self._connect()    
        except Exception as e:
            print(e)

    def _connect(self):
        try:
            print('_connect call')
            self.cnx = mysql.connector.connect(user=USER, password=PASS,
                host='127.0.0.1',
                database=DATABASE)
            self.cursor = self.cnx.cursor(dictionary=True) 
        except Exception as e:
            print(e)  
            print('connect problem')

    def _close(self):
        try:
            print('_try close')
            self.cnx.close()
        except Exception as e:
            print('close problem')
            print(e)

    def check_tables_installed(self):
        self.cursor.execute("SHOW TABLES")
        db_tables = self.cursor.fetchall()
        print(db_tables)
        if len(db_tables)>0:
            print(db_tables)
            existing = [i.pop() for i in db_tables if len(i)]
            print(existing)
            for table in TABLES:
                if table not in existing:
                    install_db_tables = True
            if install_db_tables:
                self.create_dbs()
        elif len(db_tables) == 0:
            self.create_dbs()

    def create_dbs(self):
        for table in TABLES:
            self.create_table(table)

    def create_table(self, db_name):
        try:
            if db_name in TABLES:
                self.cursor.execute(TABLES[db_name])
            self.cnx.commit()
            print(("%s created" % db_name))
        except mysql.connector.Error as err:
            print(("Failed creating database: {}".format(err)))
            #exit(1) 

    def select_all_users(self):
        try:
            self.cursor.execute(SQL['select_all_users'])
            users = self.cursor.fetchall()
            if len(users)>0:
                return users
            else:
                return None
        except Exception as e:
            print(e)

    def select_all_users_nonadmin(self):
        try:
            self.cursor.execute(SQL['select_all_users_nonadmin'])
            users = self.cursor.fetchall()
            if len(users)>0:
                return users
            else:
                return None
        except Exception as e:
            print(e)

    def select_role_by_name(self, name):
        try:
            self.cursor.execute(SQL['select_role_by_name'], {'name':name})
            role = self.cursor.fetchall()
            if len(role)>0:
                return role[0]
            else:
                return None
        except Exception as e:
            print(e)
    
    def get_user_roles(self, user_id):
        try:
            self.cursor.execute(SQL['select_user_roles_by_user_id'], {'user_id':user_id})
            roles = self.cursor.fetchall()
            if len(roles)>0:
                print(roles)
                ret = [r['name'] for r in roles]
                return ret 
            else:
                return None
        except Exception as e:
            print(e)

    def select_user_by_harvest_id(self, harvest_id):
        try:
            self.cursor.execute(SQL['select_user_by_harvest_id'], {'harvest_id':harvest_id})
            customer = self.cursor.fetchone()
            if customer:
                return customer 
            else:
                return None
        except Exception as e:
            print(e)


    def select_user_by_email(self, email):
        try:
            self.cursor.execute(SQL['select_user_by_email'], {'email':email})
            customers = self.cursor.fetchall()
            if len(customers)>0:
                return customers[0]
            else:
                return None
        except Exception as e:
            print(e)

    def update_stripe_customer_id(self, email, stripe_customer_id):
        try:
            existing = self.select_user_by_email(email)    
            if existing:
                user= {'email':email, 'stripe_customer_id':stripe_customer_id}
                self.cursor.execute(SQL['update_stripe_id_by_email'], user)
                self.cnx.commit()
                return True
            else:
                return False
        except Exception as e:
            print(e)
            self.cnx.rollback()
            return False
            print(e)
    
    def update_user_pass(self, email, password):
        try:
            existing = self.select_user_by_email(email)    
            if existing:
                user= {'email':email, 'password':password}
                self.cursor.execute(SQL['update_pass_by_email'], user)
                self.cnx.commit()
                return True
            else:
                return False
        except Exception as e:
            print(e)
            self.cnx.rollback()
            return False
            print(e)

    def create_user(self, first_name, last_name, harvest_client_id, email, password): 
        try:
            existing = self.select_user_by_email(email)    
            if existing is None:
                user = {
                        'first_name':first_name,
                        'last_name':last_name,
                        'harvest_client_id':harvest_client_id,
                        'email':email,
                        'password':password,
                        }
                self.cursor.execute(SQL['create_user'], user)
                self.cnx.commit()
                # user role
                self.add_user_role(self.cursor.lastrowid)
                return user
            else:
                return False
        except Exception as e:
            print(e)
            self.cnx.rollback()
            return False
            print(e)

    def create_role(self, role): 
        try:
            existing = self.select_role_by_name(role)    
            print(role)
            print('existing')
            print(existing)
            if existing is None:
                r = {
                    'name':role,
                    }
                self.cursor.execute(SQL['create_role'], r)
                self.cnx.commit()
                return r 
            else:
                return False
        except Exception as e:
            print(e)
            self.cnx.rollback()
            return False
            print(e)

    def add_user_role(self, user_id, role_id=2): 
        try:
            r = {
                'user_id':user_id,
                'role_id':role_id,
                }
            self.cursor.execute(SQL['add_user_role'], r)
            self.cnx.commit()
            return r 
        except Exception as e:
            print(e)
            self.cnx.rollback()
            return False
            print(e)

    def create_email_code(self, email):
        try:
            code = int(os.urandom(4).encode('hex'),16) 
            print(code)
            r = {'code':code,'email':email,'state':'open'} # states: open|closed
            self.cursor.execute(SQL['create_email_code'], r)
            self.cnx.commit()
            return code 
        except Exception as e:
            print(e)
            self.cnx.rollback()
            return False
            print(e)

    def get_email_code(self, code):
        try:
            self.cursor.execute(SQL['get_email_code'], {'code':code})
            record = self.cursor.fetchone()
            if record:
                return record
            else:
                return None
        except Exception as e:
            print(e)

    def validate_email_code(self, code):
        try:
            existing = self.get_email_code(code)
            print(existing)
            if existing and existing['state'] == 'open':
                r={'code':code}
                self.cursor.execute(SQL['close_email_code'], r)
                self.cnx.commit()       
                return existing 
        except Exception as e:
            print(e)
            self.cnx.rollback()
            return False
            print(e) 

    def insert_plaid_access_token(self, email, access_token):
        try:
            u = self.select_user_by_email(email)
            if u is not None:
                user= {'user_id':u['id'], 'access_token':access_token}
                self.cursor.execute(SQL['add_plaid_access_token'], user)
                self.cnx.commit()
                return True
            return False
        except Exception as e:
            print(e)
            self.cnx.rollback()
            return False

if __name__ == "__main__":
    db = DB()
    #db.create_table('plaid_access_tokens')
    #db.create_email_code("contact@acorninn.com")
    #db.validate_email_code('3802443375')
    #db.check_tables_installed()

 
