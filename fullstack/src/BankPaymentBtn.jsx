import React from "react";

import axios from 'axios';
import Button from 'reactstrap/lib/Button';
import Input from 'reactstrap/lib/Input';

class BankPaymentBtn extends React.Component {
    constructor(props) {
        super(props);
        console.log('BankPaymentBtn');
        console.log(props);
        this.state = {
            showAlert: false,
            due_amount: props.due_amount,
            invoice_id: props.invoice_id,
        }
        this.handleShow = this.handleShow.bind(this)
    }
    handleShow() {
        this.setState({
            showAlert: true,
        });
    }
    // returns the cookie with the given name,
    // or undefined if not found
    getCookie(name) {
      let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
      ));
      return matches ? decodeURIComponent(matches[1]) : undefined;
    }
    render() {
        if(this.state.showAlert){
            let _xsrf = this.getCookie('_xsrf');
            return (
                <div>
                    <div class="alert alert-warning" role="alert">By clicking “Submit” I authorize Bit Byte Bit, LLC to initiate, and my financial institution to honor, an electronic payment in the amount of ${this.state.due_amount} from the bank account number ******<span id="last4"></span></div>
                    <form id="payment_form" action={"/admin/invoice/"+this.state.invoice_id} method="POST">
                        <input type="hidden" name="_xsrf" value={_xsrf} />
                        <input type="hidden" name="last4" value={this.props.last4} />
                        <input type="hidden" name="token_type" value="bank_payment" />
                        <input type="hidden" name="amount" value={this.state.due_amount*100} />
                        <Button color="success">Submit</Button>
                    </form>
                </div>
            );
        } else {
            return (
                <div>
                <Button color="success" onClick={this.handleShow} >{this.props.bank_name} - {this.props.last4}</Button>
                </div>
            );
        }
    }
}

export default BankPaymentBtn;
