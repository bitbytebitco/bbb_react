import React from "react";
import { render } from 'react-dom';
import axios from 'axios';

import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
import CardTitle from "reactstrap/lib/CardTitle";
import Table from "reactstrap/lib/Table";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Container from "reactstrap/lib/Container";

const ServiceBox = React.lazy(() => import("./ServiceBox"));
const Tagline = React.lazy(() => import("./Tagline"));

const SectionContent = (props) => {
    const title = props.title;
    const content = props.content;
    const iconClasses = props.iconClasses;
    console.log(props)
    if(content != null){
        return (
            <React.Fragment>
                <Row className="mt-3 mb-3 py-2">
                    <ServiceBox heading={props.title} image={props.image} iconClasses={iconClasses} col_size="4" />
                    <Col md="8">
                        <div dangerouslySetInnerHTML={{__html:content}} className="text-muted mt-2" />
                    </Col>
                </Row>
            </React.Fragment>
        ) 
    } else {
        return null;
    }
}

export default class Projects extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            title: null,
            tagline: null,
            well_data: null,
            projects: [],
        }
        console.log('&&&&&'); 
        console.log(this.props.section); 
        axios.get('/service_info/' + this.props.section)
        .then( res => {
            console.log(res.data);
            let sorted = res.data.projects.sort((a,b) => {
                    console.log(typeof(a.priority));
                    return a.priority - b.priority;
                });
            this.setState({
                projects : sorted
            });
            if(res.data.title != null && res.data.well_data != null){
                console.log('setting title and well_data');
                this.setState({
                    title:res.data.title,
                    well_data:res.data.well_data,
                });   
            }
            if(res.data.tagline != null){
                this.setState({
                    tagline:res.data.tagline,
                });
            }
            if(res.data.image != null){
                this.setState({
                    image:res.data.image,
                });
            }
            if(res.data.icon_classes != null){
                this.setState({
                    icon_classes:res.data.icon_classes,
                });
            }
            console.log(sorted);
        });
    }
    
    render () {
        return (
            
            <div>
                <Tagline tagline={this.state.tagline} /> 
                <div> 
                    <Container>
                        <Row className="lead">
                            <Col>
                                <SectionContent content={this.state.well_data} title={this.state.title} image={this.state.image} iconClasses={this.state.icon_classes} />
                            </Col>
                        </Row>
                    </Container>
                </div>
                <Container className="mb-5 mt-2">
                    <Col>
                        <center><h2 className="tagline">Projects</h2></center> 
                    {this.state.projects.map((project, i) =>
                        <ProjectSimple {...project} />    
                    )}
                    </Col>
                </Container>
            </div>
        )
    }
}
const LogoImg = (props) => {
    if(props.logo){
        console.log('');
        console.log('LogoImg');
        console.log(props);
        var logo_class = ""
        if(props.logo_class){
            var logo_class = props.logo_class;
            console.log(logo_class);
        }
        return <center><img src={require('../static/images/'+props.logo)} alt="Logo" className={"img-fluid rounded-0 " + logo_class} /></center>
    } else {
        console.log('undefined');
        return null;
    }
}
const ProjectTitle = (props) => {
    if(props.url) {
        var url = <a target="_blank" href={props.url}><h5 className="pb-3">{props.name}</h5></a>
    } else {
        var url = <h5 className="pb-3">{props.name}</h5>
    }
    if(props.name && !props.logo) {
        return (
            <center>
                { url }     
            </center>
        ) 
    } else {
        return null;
    }
}
const ProjectButtons = (props) => {
    const url = props.url ? <a target="_blank" href={props.url} class="d-inline-block w-50 border project-button" >View Site</a> : null;
    return (
        <div class="mt-auto p-2 w-100">
            <a href={"/project/"+ props.id} class="d-inline-block w-50 border project-button" >Details</a>
            { url }
        </div>
    ) 
}
const ProjectSimple = (props) => {
    console.log("ProjectSimple")
    return (
        <React.Fragment>
            <Col md="4" className="shadow float-left">
                <Card id={"project-" + props.id} className="mb-4 project-simple">
                <CardBody className="d-flex align-items-start flex-column">
                    <div class="p-2 w-100">
                        <LogoImg { ...props } />
                        <ProjectTitle { ...props} /> 
                    </div>
                    <ProjectButtons { ...props} /> 
                </CardBody>
                </Card>
            </Col>
        </React.Fragment>
    )
}

