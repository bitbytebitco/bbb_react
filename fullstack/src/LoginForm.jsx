import React from "react";
import { Redirect } from 'react-router';

import Container from "reactstrap/lib/Container";
import Row from "reactstrap/lib/Row";
import Col from "reactstrap/lib/Col";
import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
import CardTitle from "reactstrap/lib/CardTitle";
import Form from "reactstrap/lib/Form";
import FormGroup from "reactstrap/lib/FormGroup";
import Label from "reactstrap/lib/Label";
import Input from "reactstrap/lib/Input";
import Button from "reactstrap/lib/Button";
import Alert from "reactstrap/lib/Alert";

import getCookie from "./getCookie";
//const hasUser = React.lazy(() => import("./hasUser"));

const Warning = (props) => {
    const loginFail = props.attempt;
    const message = props.message;
    if(loginFail){
        return (
            <Alert color="warning" className="mt-2">
                { message }
            </Alert>
        )
    } else {
        return null;
    }
}

//const LoginForm = () => { 
class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            loginFail: false,
            loginError: null,
            hasUser: props.hasUser,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleFail(json){
        this.setState({
            loginFail:true,
            loginError:json.error,
        });
    }
    handleSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);
        data.append("_xsrf", getCookie("_xsrf")); 
        fetch('/login', {
          method: 'POST',
          body: data,
        }).then((response) => {
            console.log(response);
            if(response.status == 403){
                console.log('fail');
                response.json().then((json) => this.handleFail(json)) ;
            } else if (response.status == 200){
                console.log('redirect');
                location.href="/admin";
            }
        });
    }
    
    render(){
        if (this.state.hasUser) {
            return <Redirect to='/admin' />
        }
        return (
            <React.Fragment>
                <Container>
                    <Row>
                        <Col md="6" className="my-3">
                            <Card>    
                                <CardBody>    
                                    <CardTitle><h2>Login</h2></CardTitle>
                                    <Form onSubmit={this.handleSubmit}>
                                        <FormGroup>
                                            <Label for="email">Email</Label>
                                            <Input type="email" name="email" id="email" placeholder="foo@bar.com" />
                                        </FormGroup>
                                        <FormGroup>
                                            <Label for="password">Email</Label>
                                            <Input type="password" name="password" id="password" placeholder="with a placeholder" />
                                        </FormGroup>
                                        <Button>Submit</Button>
                                        <Warning attempt={this.state.loginFail} message={this.state.loginError} />         
                                    </Form>
                                </CardBody>
                            </Card>    
                        </Col>
                        <Col md="6"></Col>
                    </Row>
                </Container>
            </React.Fragment>
        )
    }
}

export default LoginForm;
