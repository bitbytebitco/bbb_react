import React from "react";

import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Container from "reactstrap/lib/Container";

const Tagline = ( props ) => ( 
    <React.Fragment>
        <div className="background">
            <Container className="mt-0 pt-0"> 
                <Row className="py-3 lead text-white">
                    <Col className="d-flex justify-content-center">
                       <h2 className="my-0 tagline">{ props.tagline }</h2> 
                    </Col>
                </Row>
            </Container> 
        </div> 
    </React.Fragment>
);

export default Tagline;
