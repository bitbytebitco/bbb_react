import React from 'react';

import Navbar from "reactstrap/lib/Navbar";
import Nav from "reactstrap/lib/Nav";
import NavItem from "reactstrap/lib/NavItem";
import NavLink from "reactstrap/lib/NavLink";
import Row from "reactstrap/lib/Row";
import Col from "reactstrap/lib/Col";
import Container from "reactstrap/lib/Container";

const Footer = () => {
    return ( 
        <footer>
            <Container> 
                <Row>
                    <Col>
                        <Navbar light expand="lg" className="mx-0 px-0 py-0 my-0 navbar-expand">
                            <Nav className="mr-auto w-100" navbar>
                                <NavItem>
                                    <NavLink href="mailto:bitbytebitco@gmail.com" className="">
                                        <i className="mx-auto px-auto my-auto py-auto fas fa-envelope "></i>&nbsp;
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="https://www.linkedin.com/company/bit-byte-bit-llc/about/" className="">
                                        <i className="mx-auto px-auto fab fa-linkedin-in "></i>&nbsp;
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="tel:(804) 404-3328" className="ml-0 pl-0" >
                                        <i className="ml-0 pt-1 my-auto py-auto fas fa-phone "> </i>
                                    </NavLink>
                                </NavItem>
                                <NavItem className="ml-auto">
                                    <NavLink className="mr-0 pr-0" >
                                        &copy; 2019 
                                    </NavLink>
                                </NavItem>
                            </Nav>
                        </Navbar>
                        {/*<Row className="py-2">
                            <Col>
                                <i className="mr-2 px-auto fas fa-calendar-alt "> </i> 
                                Tuesday-Saturday, 10-6 (MST)
                            </Col>
                        </Row>*/}
                    </Col>
                </Row>
            </Container>
        </footer>
    );
}

export default Footer; 
