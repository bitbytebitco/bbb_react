import React from "react";

const Navigation = React.lazy(() => import("./Navigation"));
const About = React.lazy(() => import("./About"));
const Projects = React.lazy(() => import("./Projects"));
const Footer = React.lazy(() => import("./Footer"));

/*const ServiceInfo = () => {
    return (
        
    )
}*/

const Service = ({ match }) => {
    console.log('match');
    console.log(match);
    return (
        <div className="App Site">
            <div className="Site-content">
                <Navigation loggedIn="" /> 
                <Projects section={match.params.service} />
            </div> 
            <Footer />
        </div> 
    )
}

export default Service;
