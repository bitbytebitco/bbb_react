import React from "react";

import axios from 'axios';

import Breadcrumb from "reactstrap/lib/Breadcrumb";
import BreadcrumbItem from "reactstrap/lib/BreadcrumbItem";
import Collapse from "reactstrap/lib/Collapse";
import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
import CardTitle from "reactstrap/lib/CardTitle";
import Table from "reactstrap/lib/Table";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Container from "reactstrap/lib/Container";

const ClientList = (props) => {
    if(props.clients != null){
        let clientlist = props.clients.map((c) => {
            return <Client {...c} />
        }) 
        return clientlist;
    }
    return null;
}

const Client = (props) => { 
    return <tr><td><a href={"/admin/client/"+props.id}>{ props.name }</a></td></tr>; 
}

class AdminClients extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            clients:[]
        }
    }
    componentDidMount(){
        console.log('test');
        axios
            .get(
                    '/admin/clients/info',
                    { withCredentials: true }
                )
            .then(res => {
                console.log(res.data)
                let clients = [] 
                res.data.map((a)=>{
                    clients.push(a); 
                })
                this.setState({clients:clients})
            })
            .catch(err => { 
                console.log(err);
            });
    }
    render(){
        return (
            <React.Fragment>
            <Container>
                <Row>
                    <Col>
                        <Breadcrumb className="mt-3" >
                            <BreadcrumbItem>Admin</BreadcrumbItem>
                        </Breadcrumb>
                    </Col>
                </Row>
            </Container>
            <Container>
                <Row>
                    <Col md="3">
                        <Card>    
                            <CardBody>    
                                <CardTitle>
                                    <h5>Something</h5>
                                </CardTitle>
                                <p></p> 
                            </CardBody>    
                        </Card>    
                    </Col>
                    <Col md="9">
                        <Card>    
                            <CardBody>    
                                <CardTitle>
                                    <h5 className="text-left w-100">Clients</h5>
                                </CardTitle>
                                <div class="table-responsive">
                                    <Table className="bg-transparent mb-0">
                                        <tbody>
                                        <ClientList clients={this.state.clients} />
                                        </tbody>
                                    </Table>
                                </div>
                            </CardBody>    
                        </Card>    
                    </Col>
                </Row>
            </Container>
            </React.Fragment>
        )
        
    }
}


export default AdminClients;
