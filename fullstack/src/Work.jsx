import React from "react";

const Navigation = React.lazy(() => import("./Navigation"));
const About = React.lazy(() => import("./About"));
const Projects = React.lazy(() => import("./Projects"));
const Footer = React.lazy(() => import("./Footer"));

const Work = () => {
    return (
        <div className="App Site">
            <div className="Site-content">
                <Navigation /> 
                <Projects section="all" />
            </div> 
            <Footer />
        </div> 
    )
}

export default Work;
