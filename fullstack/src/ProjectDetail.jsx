import React from "react";
import axios from 'axios';

import ImageGallery from 'react-image-gallery';

import Table from "reactstrap/lib/Table";
import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Container from "reactstrap/lib/Container";

const Tagline = React.lazy(() => import("./Tagline"));

class ProjectDetail extends React.Component {
    constructor(props) {
        super(props); 
        this.state = {
            screenshots: [],
        }
        console.log('ProjectDetail');
        axios.get('/json/project/' + this.props.project_id)
            .then( res => {
                console.log(res.data);
                console.log(res.data.url);
                let images = []
                res.data.screenshots.map((a) => {
                    images.push({"original":"/img/" + a})
                })
                
                this.setState({
                    id: res.data.id,
                    name: res.data.name,
                    descr: res.data.descr,
                    url: res.data.url,
                    logo: res.data.logo,
                    screenshots: images,
                    section: res.data.section,
                    stack: res.data.stack,
                    sub: res.data.sub,
                });
            });

    }
    componentDidMount() {
    }

    getDescription() {
        return { __html: this.state.descr};
    }
    render() {
        return (
            <div>
                <Tagline tagline={this.state.name} /> 
                <div> 
                    <Container className="pb-5 mt-4">
                        <Row className="lead">
                            <Col md="8" className="project-item">
                                <ImageGallery items={this.state.screenshots} showFullscreenButton={false} showPlayButton={false} showThumbnails={false} />
                                <p class="mt-4" dangerouslySetInnerHTML={ this.getDescription() }></p> 
                            </Col>
                            <Col md="4" >
                                <div class="row">
                                    { this.state.url ? <a target="_blank" href={this.state.url} class="p-2 mb-2 d-inline-block w-100 border project-button" >View Site</a> : null }
                                </div>
                                <TechStack {...this.state} />              
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        )
    }
}

const TechStack = (props) => {
        console.log(props);
        if(props.sub){
            var services = props.sub.split(',').map((a, i) => {
                return (<tr className="my-0 py-0"><td>{a}</td></tr>);
            });
        } else {
            var services = ""
        }
        if(props.stack){
            var tech = Object.entries(props.stack).map((a) => {
                return (<tr className="my-0 py-0"><td><b>{a[0]}</b></td><td>{ a[1] }</td></tr>)
            });
        } else {
            var tech = "";
        }
        return (
            <div>
            <Row>
                <Col className="card card-body">
                <h5 className="text-left w-100">Project Scope</h5>
                <Table className="bg-transparent">
                    <tbody>
                        { services }
                    </tbody>
                </Table>
                </Col>
            </Row>
            <Row>
                <Col className="card card-body mt-4">
                <h5 className="text-left w-100">Technology Used</h5>
                <Table className="bg-transparent">
                    <tbody>
                        { tech }
                    </tbody>
                </Table>
                </Col>
            </Row>
            </div> 
        );
}

export default ProjectDetail;
