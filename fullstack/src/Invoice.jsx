import React from "react";

import axios from 'axios';

const BankPaymentBtn = React.lazy(() => import("./BankPaymentBtn"));

import Button from 'reactstrap/lib/Button';
import Breadcrumb from "reactstrap/lib/Breadcrumb";
import BreadcrumbItem from "reactstrap/lib/BreadcrumbItem";
import Collapse from "reactstrap/lib/Collapse";
import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
import CardTitle from "reactstrap/lib/CardTitle";
import Table from "reactstrap/lib/Table";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Container from "reactstrap/lib/Container";


class Invoice extends React.Component {
    constructor() {
        super();
        this.state = {
            invoice:{},
            payment_methods:{},
        }
    }
    componentDidMount() {
        const invoice_id = this.props.invoice_id;
        axios
            .get(
                '/admin/invoice_info/'+invoice_id,
                { withCredentials: true }
            )
            .then(res => {
                console.log('test'); 
                console.log(res.data); 
                this.setState({
                    invoice:res.data,
                });
            })
            .catch(err => {
                console.log(err);
            });

        axios
            .get(
                '/admin/payment_methods',
                { withCredentials: true }
            )
            .then(res => {
                console.log('payment_methods response'); 
                console.log(res.data); 
                console.log(res.data.bank_accts); 
                this.setState({
                    payment_methods:res.data,
                });
            })
            .catch(err => {
                console.log(err);
            });
    }
    render() {
        return (
            <React.Fragment>
                <Container>
                    <Row>
                    <Col>
                        <Breadcrumb className="mt-3" >
                            <BreadcrumbItem>Invoice</BreadcrumbItem>
                            <BreadcrumbItem active>{this.state.invoice['subject']}</BreadcrumbItem>
                        </Breadcrumb>
                    </Col>
                </Row>
                </Container>
                <Container>
                <Row>
                    <Col md="4">
                        <Card>    
                            <CardBody>    
                                <CardTitle>
                                    <h5>Invoice</h5>
                                </CardTitle>
                                <div class="table-responsive">
                                    <Table className="bg-transparent mb-0">
                                        <tr><th scope="row">Number</th><td>{ this.state.invoice['number'] }</td></tr>
                                        <tr><th scope="row">Issued</th><td>{ this.state.invoice['issue_date'] }</td></tr>
                                        <tr><th scope="row">Due</th><td>{ this.state.invoice['due_date'] }</td></tr>
                                    </Table>
                                </div>
                            </CardBody>    
                        </Card>    
                    </Col>
                    <Col md="8">
                        <Card>    
                            <CardBody>    
                                <CardTitle>
                                    <h5 className="text-left w-100">Line Items</h5>
                                </CardTitle>
                                <InvoiceItems invoice={this.state.invoice} />
                            </CardBody>    
                        </Card>    
                        <Card className="mt-2">    
                            <CardBody>    
                                <CardTitle>
                                    <h5 className="text-left w-100">Total</h5>
                                </CardTitle>
                                <table class="table">
                                    <tr><th scope="row">Amount Due:</th><td>$ {this.state.invoice.due_amount}</td></tr>
                                </table>
                                <BankOpts invoice_id={this.state.invoice.id} due_amount={this.state.invoice.due_amount} opts={this.state.payment_methods} />
                            </CardBody>    
                        </Card>    
                    </Col>
                </Row>
            </Container>
            </React.Fragment>
        )
    }
}
const InvoiceItems = (props) => {
    console.log('InvoiceItems');
    console.log(props);
    if(props.invoice.line_items != null) {
        let itemList = props.invoice.line_items.map((i) => {
            console.log(i);
            return (
                <tr>
                    <td>{i.kind}</td>   
                    <td>{i.description}</td>   
                    <td>{i.quantity}</td>   
                    <td>${i.unit_price}</td>   
                    <td>${i.amount}</td>   
                </tr>
           ) 
        }) 
        return (
            <div class="table-responsive">
                <Table className="bg-transparent mb-0">
                    <tr>
                        <th>Item Type</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Amount</th>
                    </tr>
                    <tbody>
                        {itemList}                
                    </tbody>
                </Table>
            </div>
        )
    } else {
        return (<div></div>);
    }
}

const BankOpts = (props) => {
    console.log('PaymentOptions');
    console.log(props);
    if(props.opts != null && props.opts.bank_accts != null) {
        let bankList = props.opts.bank_accts.data.map((i) => {
            return (
               <p><BankPaymentBtn invoice_id={props.invoice_id} due_amount={props['due_amount']} last4={i.last4} bank_name={i.bank_name} /></p>
            )
        }) 
        return (<div class="mt-2">{bankList}</div>) 
    } else {
        return(<div></div>);
    }
}

                                
export default Invoice;
