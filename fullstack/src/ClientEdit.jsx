import React from "react";

import axios from 'axios';

import Button from 'reactstrap/lib/Button';
import Breadcrumb from "reactstrap/lib/Breadcrumb";
import BreadcrumbItem from "reactstrap/lib/BreadcrumbItem";
import Collapse from "reactstrap/lib/Collapse";
import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
import CardTitle from "reactstrap/lib/CardTitle";
import Table from "reactstrap/lib/Table";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Container from "reactstrap/lib/Container";
import Label from "reactstrap/lib/Label";
import Input from "reactstrap/lib/Input";
import FormGroup from "reactstrap/lib/FormGroup";
import ListGroup from "reactstrap/lib/ListGroup";
import ListGroupItem from "reactstrap/lib/ListGroupItem";


class ClientEdit extends React.Component {
    constructor() {
        super()
        this.state = {
            first_name: "",
            last_name:"",
            address:"",
            phone_office:"",
            bank_accounts:[],
            showBankAcctForm:false,
        }
    
        this.showAddBankAcctForm = this.showAddBankAcctForm.bind(this)
    } 
    componentDidMount(){
        this.setState({client_id:this.props.client_id}) 
        const client_id = this.props.client_id;
        //console.log('test');
        //console.log(this.props);
        axios
            .get(
                    '/admin/client_info/'+client_id,
                    { withCredentials: true }
                )
            .then(res => {
                console.log(res.data);
                console.log('');
                console.log('');
                console.log('address');
                 
                let data = {
                    full_name:res.data['name'],
                    first_name:res.data.contact.first_name,
                    last_name:res.data.contact.last_name,
                    address:res.data.address,
                    phone_office:res.data.contact.phone_office,
                    bank_accounts:res.data.payment_methods.bank_accts.data,
                }
                this.setState(data)
            })
            .catch(err => { 
                console.log(err);
            });
    }
    showAddBankAcctForm() {
        console.log('toggle showAddBankAcctForm');
        let showState = !this.showBankAcctForm; 
        console.log(showState);
        this.setState({
            showBankAcctForm: showState,
        }) 
    }
    render() {
        return (
            <React.Fragment>
            <Container>
                <Row>
                    <Col>
                        <Breadcrumb className="mt-3" >
                            <BreadcrumbItem>Admin</BreadcrumbItem>
                            <BreadcrumbItem active>{this.state.full_name}</BreadcrumbItem>
                        </Breadcrumb>
                    </Col>
                </Row>
            </Container>
            <Container>
                <Row>
                    <Col md="7">
                        <Card>    
                            <CardBody>    
                                <CardTitle>
                                    <h5>Address</h5>
                                </CardTitle>
                                <ClientForm {...this.state} />
                            </CardBody>    
                        </Card>    
                    </Col>
                    <Col md={{ size:5 }}>
                        <Card>    
                            <CardBody>    
                                <CardTitle>
                                    <h5 className="text-left w-100">Payment Methods</h5>
                                    <Button onClick={ this.showAddBankAcctForm } >Add Bank Account</Button>
                                    { this.state.showBankAcctForm ? 
                                    <AddBankAcctForm /> 
                                    :
                                    <div></div>
                                    }
                                    <h6 className="mt-2"><b>Bank Accounts</b></h6>
                                    <BankAccounts bank_accounts={this.state.bank_accounts} />
                                </CardTitle>
                            </CardBody>    
                        </Card>    
                    </Col>
                </Row>
            </Container>
            </React.Fragment>
        )
    }
}
function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

const ClientForm = (props) => {
    console.log('ClientForm');
    console.log(props);
    let _xsrf = getCookie('_xsrf');
    return (
        <>
        <form action={"/admin/client/" + props.client_id + "/edit"} method="post" class="form-horizontal">
            <input type="hidden" name="_xsrf" value={_xsrf} />
            <Row form>
                <Col md="5">
                    <div class="form-group">
                        <label for="first_name">First Name:</label>
                        <input type="text" class="form-control" name="first_name" value={props.first_name} />
                    </div>
                </Col>
                <Col md="6" sm={{offset:1 }}>
                    <div class="form-group">
                    <label for="first_name">Last Name:</label>
                        <input type="text" class="form-control" name="last_name" value={props.last_name} />
                    </div>
                </Col>
                <Col md="12">
                    <div class="form-group">
                        <label for="first_name">Phone (Office):</label>
                        <input type="tel" class="form-control" name="phone_office" value={props.phone_office} />
                    </div>
                </Col>
                <Col md="12">
                    <div class="form-group">
                        <label for="address">Address:</label>
                        <textarea rows="4" class="form-control " name="address" value={props.address} >{props.address}</textarea>
                    </div>
                </Col>
                <Col md="12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                    </div> 
                </Col>
            </Row>
        </form>
        </>
    )
}
const BankAccounts = (props) => {
    console.log('BankAccounts');
    console.log(props);
    if(props.bank_accounts){
        let bank_accounts = props.bank_accounts.map((i) => {
            return (
                <ListGroupItem tag="a" href="#">
                {i.bank_name } - <b>{i.last4}</b>
                </ListGroupItem>
            )});
        return (
            <>
            <ListGroup horizontal>
                {bank_accounts} 
            </ListGroup> 
            </>
        );
    } else {
        return null;
    }
}

const AddBankAcctForm = (props) => {
    return (
        <>
            <h4>Add Bank Account</h4>
            <form method="post" id="bank-account-form" >
                <input type="hidden" name="token" />	
                <input type="hidden" name="add_bank_acct" value="1" />
                <FormGroup>
                    <Label for="routing-number" class="control-label">
                    Routing Number
                    </Label>
                    <input type="text" class="form-control" id="routing-number" />
                </FormGroup>
                <FormGroup>
                    <Label for="account-number" class="control-label">
                    Account Number
                    </Label>
                    <input type="text" class="form-control" id="account-number" />
                </FormGroup>
                <FormGroup>
                    <Label for="account-holder-name" class="control-label">
                    Account Holder Name 
                    </Label>
                    <input type="text" class="form-control" id="account-holder-name" />
                </FormGroup>
                <FormGroup>
                    <Label for="account-type" class="control-label">
                    Account type
                    </Label>
                    <select id="account-type" class="form-control">
                        <option>individual</option>
                        <option>company</option>
                    </select>
                </FormGroup>
                <div id="account-errors" role="alert"></div>

                <Button color="primary">Add Account</Button>
            </form> 
        </>
    );
}
export default ClientEdit;
