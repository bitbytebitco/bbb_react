import React from "react";
import { render } from 'react-dom';

import Container from "reactstrap/lib/Container";
import Row from "reactstrap/lib/Row";
import Col from "reactstrap/lib/Col";

import ContactBar from "./ContactBar"

export default class TopBar  extends React.Component {
    render () {
        return (
            <div id="top_bar" className="py-2 background">
                <Container>
                    <Row> 
                        <Col> 
                            <ContactBar />
                        </Col> 
                    </Row> 
                </Container>
            </div>
        )
    }
}
