import React from "react";

import Row from "reactstrap/lib/Row";
import Col from "reactstrap/lib/Col";
import Container from "reactstrap/lib/Container";

const TechIcons = () => {
    return (
        <Container> 
            <Row className="pb-4 my-4">
                <Col  md={{ size: 10, offset:1  }}>
                    <div className="row tech-icons d-flex justify-content-center">
                        <img className="tech-icon " src="/img/Linux_Foundation_logo.png" />
                        <img className="tech-icon" src="/img/Python-Logo-Transparent.png" />
                        <img className="tech-icon" src="/img/react.png" />
                        <img className="tech-icon" src="/img/js.sh.png" />
                        <img className="tech-icon" src="/img/2000px-Git-logo.svg.png" />
                        <img className="tech-icon" src="/img/android.png" />
                    </div>
                </Col>
            </Row>
        </Container> 
    )
}
export default TechIcons;
