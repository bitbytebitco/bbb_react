import React from "react";
import { render } from 'react-dom';

import Collapse from "reactstrap/lib/Collapse";
import Navbar from "reactstrap/lib/Navbar";
import NavbarToggler from "reactstrap/lib/NavbarToggler";
import NavbarBrand from "reactstrap/lib/NavbarBrand";
import Nav from "reactstrap/lib/Nav";
import NavItem from "reactstrap/lib/NavItem";
import NavLink from "reactstrap/lib/NavLink";
import UncontrolledDropdown from "reactstrap/lib/UncontrolledDropdown";
import DropdownToggle from "reactstrap/lib/DropdownToggle";
import DropdownMenu from "reactstrap/lib/DropdownMenu";
import DropdownItem from "reactstrap/lib/DropdownItem";
import Row from "reactstrap/lib/Row";
import Col from "reactstrap/lib/Col";
import Button from "reactstrap/lib/Button";
import Container from "reactstrap/lib/Container";

import TopBar from "./TopBar"

const LoginLogout = (props) => {
    console.log('login/logout');
    console.log(props);
    console.log(this);
    if(!props.loggedIn) {
        return (
            <NavItem>
                <NavLink href="/login">Login</NavLink>
            </NavItem>
        )
    } else {
        return (
            <React.Fragment>
                <NavItem>
                    <NavLink href="/admin">Admin</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/logout">Logout</NavLink>
                </NavItem>
            </React.Fragment>
        )
    }
}
export default class Navigation extends React.Component {
    constructor(props) {
        super(props);
        console.log('check loggedIn');
        console.log(props);
        let loggedIn = props.loggedIn;
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
            loggedIn:loggedIn,
        };
    }
    toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
    } 
    render () {
        return (
            <div id="header" className="background">
            <TopBar />
            <Container className="background mb-0 pb-0">
                <Row> 
                    <Col className="mb-0 pb-0 "> 
                        <Navbar color="transparent" light expand="md" className="mx-0 px-0">
                            <NavbarBrand href="/" className="mx-0">
                                <img src={require('../static/images/bbb_logo_2.png')} alt="Logo"
                                        className="img-fluid rounded-0" />
                            </NavbarBrand>
                            <NavbarToggler onClick={this.toggle} />
                            <Collapse isOpen={this.state.isOpen} navbar>
                                <Nav className="ml-auto lead" navbar>
                                    {/*<NavItem>
                                        <NavLink href="/work">Work</NavLink>
                                    </NavItem>*/}
                                    <UncontrolledDropdown className="nav-item">
                                        <DropdownToggle tag="a" className="nav-link" caret>
                                            Work 
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem href="/service/web">Web Development</DropdownItem>
                                            <DropdownItem href="/service/saas">SaaS</DropdownItem>
                                            <DropdownItem href="/service/data-vis">Data Visualization</DropdownItem>
                                            <DropdownItem href="/service/api">API Integration</DropdownItem>
                                            <DropdownItem href="/service/data-scraping">Data Scraping</DropdownItem>
                                            <DropdownItem href="/service/iot">Internet of Things</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                    <NavItem>
                                        <NavLink href="/contact">Contact</NavLink>
                                    </NavItem>
                                    <LoginLogout loggedIn={this.state.loggedIn} />
                                    {/*<UncontrolledDropdown nav inNavbar>
                                        <DropdownToggle nav caret>
                                            Options
                                        </DropdownToggle>
                                        <DropdownMenu right>
                                            <DropdownItem>
                                            Option 1
                                            </DropdownItem>
                                            <DropdownItem>
                                            Option 2
                                            </DropdownItem>
                                        <DropdownItem divider />
                                            <DropdownItem>
                                            Reset
                                            </DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>*/}
                                </Nav>
                            </Collapse>
                        </Navbar>
                    </Col> 
                </Row> 
            </Container>
            </div>
        )
    }
}
