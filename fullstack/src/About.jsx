import React from "react";

import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Container from "reactstrap/lib/Container";

const ServiceBox = React.lazy(() => import("./ServiceBox"));
const Tagline = React.lazy(() => import("./Tagline"));

const About = () => (
    <React.Fragment>
        <Tagline tagline="Building better software bit by bit." />        
        <div className="">
            <Container className="mt-3 pt-0"> 
                <Row className="py-3 lead text-muted">
                    <ServiceBox heading="Web Development" link="/service/web" image="web_development_md.png" classes="fas fa-desktop homeicon" />
                    <ServiceBox heading="SaaS" link="/service/saas" image="saas_md.png" classes="fas fa-cogs homeicon" />
                    <ServiceBox heading="Data Visualization" link="/service/data-vis" image="data_analytics_md.png" classes="fas fa-signal homeicon" />
                </Row>
                <Row className="py-3 lead text-muted">
                    <ServiceBox heading="API Integration" link="/service/api" image="api_md.png" classes="fab fa-connectdevelop homeicon" />
                    <ServiceBox heading="Data Scraping" link="/service/data-scraping" image="data_scraping_md.png" classes="fa fa-filter homeicon" />
                    <ServiceBox heading="Internet of Things" link="/service/iot" image="signal_processing_md.png" classes="fas fa-signature homeicon" />
                </Row>
            </Container>
        </div>
    </React.Fragment>
);


export default About;
