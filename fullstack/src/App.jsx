// App.jsx
import React, { Component, Suspense } from "react";
import {
    BrowserRouter as Router, Route, Link, Switch
} from 'react-router-dom';

//import { faEnvelope, faCheckSquare, faCoffee } from '@fortawesome/fontawesome-free-solid'

import "../node_modules/react-image-gallery/styles/scss/image-gallery.scss";
import "../node_modules/react-image-gallery/styles/css/image-gallery.css";
import "react-image-gallery/styles/css/image-gallery.css";
    
const Navigation = React.lazy(() => import("./Navigation"));
const Footer = React.lazy(() => import("./Footer"));
const Contact = React.lazy(() => import("./Contact"));

const About = React.lazy(() => import("./About"));
const Work = React.lazy(() => import('./Work'));
const Service = React.lazy(() => import('./Service'));
const LoginForm = React.lazy(() => import('./LoginForm'));
const ClientInfo = React.lazy(() => import('./ClientInfo'));
const ClientEdit = React.lazy(() => import('./ClientEdit'));
const AdminClients = React.lazy(() => import('./AdminClients'));
const TechIcons = React.lazy(() => import('./TechIcons'));
const Invoice = React.lazy(() => import('./Invoice'));


import ProjectDetail from "./ProjectDetail";

//const getCookie = React.lazy(() => import("./getCookie"));
//const hasUser = React.lazy(() => import("./hasUser"));

const getCookie = name => {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
const hasUser = () => {
    let user = getCookie("user");
    if(user != undefined){
        return true;
    } else {
        return false;
    }
}

/*const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    fakeAuth.isAuthenticated === true
      ? <Component {...props} />
      : <Redirect to={{
          pathname: '/login',
          state: { from: props.location }
        }} />
  )} />
)*/

const ContactPage = (props) => {
    return (
        <div className="App Site">
            <div className="Site-content">
                <Navigation loggedIn={ props.hasUser }/> 
                <Contact />
            </div>
            <Footer />
        </div>
    )
}

const Intro = (props) => {
    return (
        <div className="App Site">
            <div className="Site-content">
                <Navigation loggedIn={ props.hasUser } /> 
                <About />    
                <TechIcons />
            </div> 
            <Footer />
        </div> 
    );
}

const Login = (props) => {
    return (
        <div className="App Site">
            <div className="Site-content">
                <Navigation loggedIn={ props.hasUser }/> 
                <LoginForm hasUser={props.hasUser} />
            </div> 
            <Footer />
        </div> 
    );
}

const ProjectDetails = ({ match }) => {
    return (
        <div className="App Site">
            <div className="Site-content">
                <Navigation /> 
                <ProjectDetail project_id={match.params.project_id} />
            </div> 
            <Footer />
        </div> 
    );
}
const Admin = (props) => {
    return (
        <div className="App Site">
            <div className="Site-content">
                <Navigation loggedIn={ props.hasUser } /> 
                <AdminClients /> 
            </div> 
            <Footer />
        </div> 
    );
}

const AdminClientEdit = (props) => {
    console.log('test');
    console.log(props);
    return (
        <div className="App Site">
            <div className="Site-content">
                <Navigation loggedIn={props.hasUser} /> 
                <ClientEdit client_id={props.matchInfo.match.params.client_id} />
            </div> 
            <Footer />
        </div> 
    );
}

const AdminClient = (props) => {
    return (
        <div className="App Site">
            <div className="Site-content">
                <Navigation loggedIn={props.hasUser} /> 
                <ClientInfo client_id={props.matchInfo.match.params.client_id} />
            </div> 
            <Footer />
        </div> 
    );
}

class AdminInvoice extends Component {
    constructor(props) {
        super(props);
        console.log('match check');
        console.log(props);
    }
    render() {
        return (
            <div className="App Site">
                <div className="Site-content">
                    <Navigation loggedIn={this.props.hasUser} /> 
                    <Invoice invoice_id={this.props.matchInfo.match.params.invoice_id} />
                </div> 
                <Footer />
            </div> 
        );
    }
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasUser: hasUser() 
        };
        console.log(this.state);
    }
    render() {
        return (
            <Router>
                <Suspense fallback={<div>Loading...</div>}>
                    <Switch>
                        <Route exact path="/" render={(props) => <Intro {...props} hasUser={this.state.hasUser} />} />
                        <Route exact path="/work" render={(props) => <Work {...props} hasUser={this.state.hasUser} />} />
                        <Route exact path="/contact" render={(props) => <ContactPage {...props} hasUser={this.state.hasUser} />} />
                        <Route exact path="/login" render={(props) => <Login {...props} hasUser={this.state.hasUser} />} />
                        <Route path="/service/:service" render={(props) => <Service {...props} hasUser={this.state.hasUser} />} />
                        <Route path="/project/:project_id" render={(props) => <ProjectDetails {...props} hasUser={this.state.hasUser} />} />
                        <Route exact path="/admin" render={(props) => <Admin {...props} hasUser={this.state.hasUser} />} />
                        <Route exact path="/admin/client/:client_id" render={(match) => <AdminClient matchInfo={match} hasUser={this.state.hasUser} />} />
                        <Route exact path="/admin/client/:client_id/edit" render={(match) => <AdminClientEdit matchInfo={match} hasUser={this.state.hasUser} />} />
                        <Route path="/admin/invoice/:invoice_id" render={(match) => <AdminInvoice matchInfo={match} hasUser={this.state.hasUser} />} />
                    </Switch>
                </Suspense>
            </Router> 
        );
    }
}
export default App;
