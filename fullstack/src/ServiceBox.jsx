import React from "react";

import Col from "reactstrap/lib/Col";

const Graphic = (props) => {
    if(props.image != null){
        console.log(props.image);
        return (
            <a href={props.link}>
                <img src={require('../static/images/' + props.image)} alt="Logo" className="img-fluid service-image px-4 pt-0 pb-2" />
            </a>
        )
    } else if(props.classes != null) {
        return <a href={props.link}><i className={props.classes}></i></a>
    } else {
        console.log('yup');
        console.log(props);
        return null
    }       
}
const ServiceBox = (props) => { 
    return (
        <React.Fragment>
            <Col md={props.col_size ? props.col_size : 4} className="serviceBox">
                <div className="">
                    <div className="d-flex justify-content-center">
                        <Graphic link={props.link} image={props.image} classes={props.classes} /> 
                    </div>
                    <div className="d-flex justify-content-center mt-2">
                        <h3>{props.heading}</h3> 
                    </div>
                </div>
            </Col>
        </React.Fragment>
    )
}

export default ServiceBox;
