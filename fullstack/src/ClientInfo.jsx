import React from "react";

import axios from 'axios';

import Button from 'reactstrap/lib/Button';
import Breadcrumb from "reactstrap/lib/Breadcrumb";
import BreadcrumbItem from "reactstrap/lib/BreadcrumbItem";
import Collapse from "reactstrap/lib/Collapse";
import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
import CardTitle from "reactstrap/lib/CardTitle";
import Table from "reactstrap/lib/Table";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Container from "reactstrap/lib/Container";


class ClientInfo extends React.Component {
    constructor() {
        super()
        this.state = {
            admin:false,
            data: {},
            projects: [],
            invoices: [],
        }
    } 
    getCookie(name) {
      let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
      ));
      return matches ? decodeURIComponent(matches[1]) : undefined;
    }
    componentDidMount(){
        this.setState({client_id:this.props.client_id}) 
        const client_id = this.props.client_id;
        //console.log('test');
        //console.log(this.props);
        axios
            .get(
                    '/admin/client_info/'+client_id,
                    { withCredentials: true }
                )
            .then(res => {
                //console.log(res.data)
                this.setState({
                    data:res.data,
                    name:res.data['name'],
                });
                // projects
                let projects = [] 
                res.data['projects'].map((a)=>{
                    projects.push(a); 
                })
                this.setState({projects:projects})
                // invoices 
                let invoices = [] 
                res.data['invoices'].map((i)=>{
                    invoices.push(i); 
                })
                this.setState({invoices:invoices})
            })
            .catch(err => { 
                console.log(err);
            });
    }
    render() {
        return (
            <React.Fragment>
            <Container>
                <Row>
                    <Col>
                        <Breadcrumb className="mt-3" >
                            <BreadcrumbItem>Admin</BreadcrumbItem>
                            <BreadcrumbItem active>{this.state.name}</BreadcrumbItem>
                        </Breadcrumb>
                    </Col>
                </Row>
            </Container>
            <Container>
                <Row>
                    <Col md="3">
                        <Card>    
                            <CardBody>    
                                <CardTitle>
                                    <h5>Address</h5>
                                </CardTitle>
                                <p>{this.state.data['address']}</p> 
                                <a href={"/admin/client/"+this.state.client_id+"/edit" }><Button color="secondary w-100">
                                <i className="mr-2 fas fa-cog"></i>
                                Settings</Button></a>
                            </CardBody>    
                        </Card>    
                    </Col>
                    <Col md="9">
                        <Card>    
                            <CardBody>    
                                <CardTitle>
                                    <h5 className="text-left w-100">Projects</h5>
                                </CardTitle>
                                <div class="table-responsive">
                                    <Table className="bg-transparent mb-0">
                                        <tbody>
                                        <ProjectList projects={this.state.projects} />
                                        </tbody>
                                    </Table>
                                </div>
                            </CardBody>    
                        </Card>    
                        <Invoices invoices={this.state.invoices} />
                    </Col>
                </Row>
            </Container>
            </React.Fragment>
        )
    }
}
const InvoiceList = (props) => { 
    const invoices = props.invoices;
    //console.log(invoices);
    if(invoices != null){
        let ret = invoices.map((i) => {
            return <tr><td>{i['subject']}</td></tr>
        }) 
        return ret; 
    } else {
        return null; 
    }
}
const CheckOrX = props => {
    if(props.state == "open"){
        return <i className="fas fa-times" /> 
    } else if(props.state == "draft"){
        return null;
    }else {
        return <i className="fas fa-check" /> 
    }
}
const InvoiceLink = (props) => {
    if(props.inv_state == "paid"){
        return <a href={"https://bitbytebitllc.harvestapp.com/client/invoices/"+props.client_key} target="_blank" >{props.inv_state}</a>
    } else if (props.inv_state =="draft"){
        return null
    } else {
        return <a href={"/admin/invoice/"+props.invoice_id}>open</a> 
    }
}
const Invoices = (props) => { 
    const invoices = props.invoices;
    if(invoices != null){
        return (
            <Card className="mt-4">    
                <CardBody>    
                    <CardTitle>
                        <h5 className="text-left w-100">Invoices</h5>
                    </CardTitle>
                    <div class="table-responsive">
                        <Table striped className="bg-transparent">
                            <tbody>
                            {props.invoices.map((i) => {
                                console.log('');
                                console.log('invoiceLink');
                                console.log(i);   
                                return (
                                    <tr>
                                        <td>{i['subject']}</td>
                                        <td><InvoiceLink invoice_id={i['id']} inv_state={i['state']} client_key={i['client_key']} /></td> 
                                        <td><CheckOrX state={i['state']} /></td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </Table>
                    </div>
                </CardBody>    
            </Card>    
        )
    } else {
        return null;
    }
}

class ProjectListItem extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            admin:false,
            collapse: false, 
            icon:"fa-chevron-left",
            devUrl: null,
            prodUrl: null,
        }
    }
    componentDidMount() {
        if(this.props.notes != null){
            let m1 = this.props.notes.match(/dev_url:.*/g); 
            if(m1 != null){
                if(m1.length>0){
                    this.setState({
                        devUrl:m1[0].replace('dev_url:','')
                    })
                }
            }
            let m2 = this.props.notes.match(/prod_url:.*/g); 
            if(m2 != null){
                if(m2.length>0){
                    this.setState({
                        prodUrl:m2[0].replace('prod_url:','')
                    })
                }
            }
        }
    }
    toggle() {
        try {
            if(this.state.icon == "fa-chevron-left"){
                this.icon = "fa-chevron-down"
            } else {
                this.icon = "fa-chevron-left"
            }
            console.log("chevron test");
            console.log(this.icon);
            console.log(this.state);
        } catch(e){ }
        this.setState({
            collapse: !this.state.collapse,
            icon:this.icon,
        });
    }
    render(){
        return (
            <tr onClick={this.toggle} className="project-list-item">
                <td>{this.props.name}<i className={"float-right fas "+this.state.icon} />
                    <Collapse isOpen={this.state.collapse} className="mt-2" >
                        <Table className="bg-white">
                            <tbody>
                                <tr>
                                    <td><b>Dev Url:</b> {this.state.devUrl}</td>
                                    <td><b>Prod Url:</b> {this.state.prodUrl}</td>
                                </tr>
                            </tbody>
                        </Table>    
                    </Collapse>
                </td>
            </tr>
        )
    }
}

const ProjectList = (props) => { 
    const projects = props.projects;
    if(projects != null){
        console.log('not null');
        let projectList = projects.map((p) => {
            return (
                <ProjectListItem {...p} />
            )
        }) 
        return projectList; 
    } else {
        return null; 
    }
}

export default ClientInfo;
