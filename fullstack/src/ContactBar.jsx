import React from "react";
import { render } from 'react-dom';

import Navbar from "reactstrap/lib/Navbar";
import Nav from "reactstrap/lib/Nav";
import NavItem from "reactstrap/lib/NavItem";
import NavLink from "reactstrap/lib/NavLink";

export default class ContactBar  extends React.Component {
    render () {
        return (
            <div>
                <Navbar light expand="lg" className="mx-0 px-0 py-0 my-0 navbar-expand">
                    <Nav className="ml-auto" navbar>
                        {/*<NavItem>
                            <NavLink href="tel:(804) 404-3328" className="" >
                                <i className="mx-auto px-auto fas fa-phone "> </i> (804) 404-3328
                            </NavLink>
                        </NavItem>*/}
                        <NavItem>
                            <NavLink href="mailto:bitbytebitco@gmail.com" className="">
                                <i className="mx-auto px-auto my-auto py-auto fas fa-envelope "></i>&nbsp;
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="https://www.linkedin.com/company/bit-byte-bit-llc/about/" className="">
                                <i className="mx-auto px-auto fab fa-linkedin-in "></i>&nbsp;
                            </NavLink>
                        </NavItem>
                        
                        
                    </Nav>
                </Navbar>
                 
                {/*<a href="https://www.linkedin.com/company/bit-byte-bit-llc/about/" role="button" className=" text-white ">
                    <i className="w-100 mx-auto px-auto fab fa-linkedin-in justify-content-center align-self-center"></i>
                </a>
                <a href="mailto:bitbytebitco@gmail.com" role="button" className="text-white ml-2 ">
                    <i className="w-100 mx-auto px-auto my-auto py-auto fas fa-envelope justify-content-center align-self-center"></i>
                </a>*/}
            </div>
        )
    }
}
