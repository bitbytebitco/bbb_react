import React from "react";

import Row from "reactstrap/lib/Row";
import Col from "reactstrap/lib/Col";
import Container from "reactstrap/lib/Container";

const Contact = () => {
    return (
        <div>
            <Container className="mt-5"> 
                <Row className="py-3 ">
                    <Col>
                        <Row className="py-3 border-bottom">
                            <Col lg="4">
                               <h5>Contact</h5> 
                            </Col>
                            <Col lg="8">
                                <i className="mx-2 px-auto my-auto py-auto fas fa-envelope "></i> 
                                <a href="mailto:bitbytebitco@gmail.com">bitbytebitco@gmail.com</a>
                            </Col>
                        </Row>
                        <Row className="py-3 border-bottom">
                            <Col lg="4">
                               <h5>Telephone</h5> 
                            </Col>
                            <Col lg="8">
                                <i className="mx-2 px-auto fas fa-phone "> </i> 
                                <a href="tel:(804)404-3328">(804) 404-3328</a>
                                <br />
                                <i className="mx-2 px-auto fas fa-calendar-alt "> </i> 
                                Monday-Friday, 10-6 (MST)
                            </Col>
                        </Row>
                        <Row className="py-3">
                            <Col lg="4">
                               <h5>Social</h5> 
                            </Col>
                            <Col lg="8">
                                <i className="mx-2 px-auto fab fa-linkedin-in "></i>&nbsp;
                                <a href="https://www.linkedin.com/company/bit-byte-bit-llc/about/">LinkedIn</a>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col>
                       <center><h2 className="my-0 tagline">Humans</h2></center>
                    </Col>
                </Row>
                <Row className="py-3">
                    <Col lg="3">
                        <img src={require('../static/images/zach_md.png')} className="img-fluid rounded-0 mb-2" />
                        <p>
                            <center><h5>Zachary Becker</h5></center>
                            <center>Software Lead/Founder</center>
                        </p>
                    </Col>
                    <Col lg="3">
                        <img src={require('../static/images/wilson_icon.png')} className="img-fluid rounded-0 mb-2" />
                        <center><h5>Wilson Wyllie</h5></center>
                        <center>Art Direction/Graphic Design/Illustration</center>
                    </Col>
                </Row>
            </Container> 
        </div>        
    )
}

export default Contact;
