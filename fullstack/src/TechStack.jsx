import React from 'react';

import Row from "reactstrap/lib/Row";
import Col from "reactstrap/lib/Col";
import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
import CardTitle from "reactstrap/lib/CardTitle";
import Table from "reactstrap/lib/Table";

export default class TechStack extends React.Component {
    constructor(props) {
        super(props)
        console.log(this.props.project);
    }
    render(){
        return (
            <div>
            <Row>
                <Col>
                <Table className="bg-transparent mt-5">
                    <tbody>
                        <Card>    
                            <CardBody>    
                                <CardTitle>
                                    <h5 className="text-left w-100">Services Provided</h5>
                                </CardTitle>
                                {this.props.project.sub.split(',').map((a, i) => {
                                    return (<tr className="my-0 py-0"><td>{a}</td></tr>);
                                })}
                            </CardBody>    
                        </Card>    
                    </tbody>
                </Table>
                </Col>
            </Row>
            <Row>
                <Col>
                <Table className="bg-transparent mt-5">
                    <tbody>
                        <Card>    
                            <CardBody>    
                                <CardTitle>
                                    <h5 className="text-left w-100">Technology Used</h5>
                                </CardTitle>
                                {Object.entries(this.props.project.stack).map((a) => {
                                    return (<tr className="my-0 py-0"><td><b>{a[0]}</b></td><td>{ a[1] }</td></tr>)
                                })}
                            </CardBody>    
                        </Card>    
                    </tbody>
                </Table>
                </Col>
            </Row>
            </div> 
        );
    }
}
