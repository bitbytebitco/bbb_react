const webpack = require('webpack');
const path = require('path');

const buildPath = "/dist/";

const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');


const config = {
    devtool: 'source-map',
    entry: './src/index.jsx',
    output: {
        filename: '[name].bundle.js',
        chunkFilename: '[name].bundle.js', 
        path: path.resolve(__dirname + '/static/dist'),
        publicPath: buildPath,
    },
    resolve: {
        extensions: ['.js','.jsx', '.css','.scss', '.png'],
        alias: {
            '@fortawesome/fontawesome-free-solid$': '@fortawesome/fontawesome-free-solid/shakable.es.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.jsx?/,
                exclude: /node_modules/,
                use: [{
                    loader:'babel-loader?cacheDirectory',
                    options: {
                        presets: ['@babel/preset-react','@babel/env'],
                        rootMode: "upward",
                    },
                }]
            },
            { 
                test: /\.(gif|svg|jpg|png)$/,
                loader: "file-loader",
            },
            {
                test:/\.css$/,
                use:['style-loader','css-loader']
            },
            {
            test: /\.scss$/,
            use: [
                "style-loader", // creates style nodes from JS strings
                "css-loader", // translates CSS into CommonJS
                "sass-loader" // compiles Sass to CSS, using Node Sass by default
            ]
            }
        ]
    },
    optimization: {
        minimize: false,
        minimizer: [new UglifyJsPlugin({
            sourceMap: true,
        })],
        splitChunks: {
          chunks: 'all',
        },
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'BBB React',
            hash: true,
            template: './src/index.html',
            filename: '../index.html'
        }),
      ]
};

module.exports = config;


