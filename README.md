# Description 
This is the most recent version of [BitByteBit](https://bitbytebit.co), updated to make use of React. 
It is configured to be load balanced, if necessary, through nginx. This application provides 
marketing for the business but also provides a way for clients to view/accept estimates and view/pay invoices. 

## Stack Used
* Python3
* Tornado
* React
* MySQL
* Nginx


## APIs Used
* Harvest App
* Stripe
* Plaid 

## Dependencies
```python
argon2-cffi==18.3.0
backports-abc==0.5
certifi==2018.10.15
cffi==1.11.5
chardet==3.0.4
Click==7.0
configparser==3.5.0
enum34==1.1.6
flake8==3.5.0
idna==2.7
Jinja2==2.10
markdown2==2.3.6
MarkupSafe==1.0
mccabe==0.6.1
mysql-connector-python==8.0.20
passlib==1.7.1
pkg-resources==0.0.0
plaid-python==2.3.4
protobuf==3.12.2
pycodestyle==2.3.1
pycparser==2.19
pyflakes==2.0.0
PyYAML==3.13
requests==2.20.0
singledispatch==3.4.0.3
six==1.11.0
stripe==2.10.1
tornado==5.1.1
urllib3==1.24
```



